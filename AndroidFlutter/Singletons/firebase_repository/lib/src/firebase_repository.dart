import 'dart:async';
import 'dart:developer';
import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_repository/firebase_repository.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:connection_status/connection_status.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:google_maps_webservice/directions.dart';

class ParkingLot {
  final String address;
  final String id;
  final String localidad;
  GeoPoint location;
  final String name;
  final int costPerMinute;
  final String owner;
  final int parkingSpots;
  final int zipCode;

  ParkingLot(
      {@required this.address,
      @required this.id,
      @required this.localidad,
      @required this.location,
      @required this.name,
      @required this.costPerMinute,
      @required this.owner,
      @required this.parkingSpots,
      @required this.zipCode});

  String toString() =>
      "{ address: $address, id: $id, localidad: $localidad, location: $location , name: $name, costPerMinute: $costPerMinute, owner: $owner, parkingSpots: $parkingSpots, zipCode: $zipCode }";
}

class ParkingHistory {
  final Timestamp fecha;
  ParkingHistory({@required this.fecha});
}

class Reserva {
  final DateTime startHour;
  final DateTime endHour;
  final String payment;
  final String status;

  Reserva(
      {@required this.startHour,
      @required this.endHour,
      @required this.payment,
      @required this.status});
}

class Service {
  final String tipo;
  final String horaInicio;
  Service({@required this.tipo, @required this.horaInicio});
}

class Valet {
  final String numero;
  final String nombre;
  Valet({@required this.numero, @required this.nombre});
}

class ServiceValet extends Service {
  final Valet valet;
  final GeoPoint locationCar;
  final GeoPoint locationDestination;
  final steps;
  final String type;

  ServiceValet(
      {@required this.locationCar,
      @required this.locationDestination,
      @required this.type,
      @required this.steps,
      @required String horaInicio,
      @required this.valet})
      : super(horaInicio: horaInicio, tipo: "VALET");
}

class ServiceParked extends Service {
  final Valet valet;
  final GeoPoint locationCar;
  ServiceParked(
      {@required this.locationCar,
      @required String horaInicio,
      @required this.valet})
      : super(horaInicio: horaInicio, tipo: "PARKED");
}

//Clase que se encarga de manejar la persistencia local de datos.
class FireBaseRepository {
  final ConnectionStatus connectionStatus = ConnectionStatus.getInstance();
  StreamSubscription _connectionChangeStream;
  SharedPreferences sharedPrefs;
  int contador = 0; //Cuenta la cantidad de cambios que ha hecho sin internet
  Random r = new Random();
  FireBaseRepository() {
    _connectionChangeStream =
        connectionStatus.connectionChange.listen(connectionChanged);
    _connectionChangeStream.onError((error) {
      print("Erro de Firebase_connection_changed, {Error: $error } ");
    });
  }

  init() async {
    sharedPrefs = await SharedPreferences.getInstance();
    if (sharedPrefs.containsKey("fireCont")) {
      contador = sharedPrefs.getInt("fireCont");
    }
  }

  save() async {
    sharedPrefs = await SharedPreferences.getInstance();
    sharedPrefs.setInt("fireCont", contador);
  }

  void connectionChanged(dynamic hasConnection) {
    if (!hasConnection) {
      contador = 0;
    }
  }

  Future<ParkingLot> getParkingLot(String id) async {
    return await Firestore.instance
        .collection('parkings')
        .document(id)
        .get()
        .then((valu) {
      return ParkingLot(
          id: valu.documentID,
          name: valu.data["name"],
          address: valu.data["address"],
          costPerMinute: valu.data["costPerMinute"],
          location: valu.data["location"],
          localidad: valu.data["localidad"],
          owner: valu.data["owner"],
          parkingSpots: valu.data["parkingSpots"],
          zipCode: valu.data["zipCode"]);
    });
  }

  Future<void> deleteParkingLot(ParkingLot parkingLot) async {
    if (!connectionStatus.hasConnection) {
      contador++;
      if (contador > 3) {
        //Esto se verfica directamente desde el dismissable
        throw ("Por favor Intente más Tarde!");
      }
      Firestore.instance
          .collection('parkings')
          .document(parkingLot.id)
          .delete();
    } else {
      await Firestore.instance
          .collection('parkings')
          .document(parkingLot.id)
          .delete();
    }
  }

  Future<dynamic> addParkingLot(ParkingLot parkingLot) async {
    if (!connectionStatus.hasConnection) {
      contador++;
      if (contador > 3) {
        throw ("Por favor intente más tarde!");
      }
      Firestore.instance.collection('parkings').document().setData({
        'name': parkingLot.name,
        'address': parkingLot.address,
        'costPerMinute': parkingLot.costPerMinute,
        'location': GeoPoint(4.6 + (4.711 - 4.6) * r.nextDouble(),
            -74.06 + (-74 + 74.06) * r.nextDouble()),
        'localidad': parkingLot.localidad,
        'owner': parkingLot.owner,
        'parkingSpots': parkingLot.parkingSpots,
        'zipCode': parkingLot.zipCode
      });
    } else {
      //Geoflutterfire geo = Geoflutterfire();??
      await Firestore.instance.collection('parkings').document().setData({
        'name': parkingLot.name,
        'address': parkingLot.address,
        'costPerMinute': parkingLot.costPerMinute,
        'location': GeoPoint(4.6 + (4.711 - 4.6) * r.nextDouble(),
            -74.06 + (-74 + 74.06) * r.nextDouble()),
        'localidad': parkingLot.localidad,
        'owner': parkingLot.owner,
        'parkingSpots': parkingLot.parkingSpots,
        'zipCode': parkingLot.zipCode
      });
    }
  }

  Future<dynamic> editParkingLot(ParkingLot parkingLot) async {
    if (!connectionStatus.hasConnection) {
      contador++;
      if (contador > 3) {
        throw ("Por favor Intente más Tarde!");
      }
      Firestore.instance
          .collection('parkings')
          .document(parkingLot.id)
          .setData({
        'name': parkingLot.name,
        'address': parkingLot.address,
        'costPerMinute': parkingLot.costPerMinute,
        'location': parkingLot.location,
        'localidad': parkingLot.localidad,
        'owner': parkingLot.owner,
        'parkingSpots': parkingLot.parkingSpots,
        'zipCode': parkingLot.zipCode
      });
    }
    //Geoflutterfire geo = Geoflutterfire();
    await Firestore.instance
        .collection('parkings')
        .document(parkingLot.id)
        .setData({
      'name': parkingLot.name,
      'address': parkingLot.address,
      'costPerMinute': parkingLot.costPerMinute,
      'location': parkingLot.location,
      'localidad': parkingLot.localidad,
      'owner': parkingLot.owner,
      'parkingSpots': parkingLot.parkingSpots,
      'zipCode': parkingLot.zipCode
    });
  }

  Future<List<ParkingLot>> getParkingLots() async {
    QuerySnapshot querySnapshot =
        await Firestore.instance.collection("parkings").getDocuments();
    List<ParkingLot> lista = [];
    querySnapshot.documents.forEach((doc) {
      lista.add(ParkingLot(
          address: doc.data['address'],
          id: doc.documentID,
          costPerMinute: doc.data['costPerMinute'],
          localidad: doc.data['localidad'],
          location: doc.data['location'],
          name: doc.data['name'],
          owner: doc.data['owner'],
          parkingSpots: doc.data['parkingSpots'],
          zipCode: doc.data['zipCode']));
    });
    return lista;
  }

  Future<Service> darServicioActual(String correo) async {
    if (!connectionStatus.hasConnection) {
      throw ("SIN CONEXION");
    }
    return await Firestore.instance
        .collection('servicios')
        .document(correo)
        .get()
        .then((valu) {
      if (valu.data == null) {
        return null;
      }
      if (valu.data["tipo"] == "VALET") {
        var steps = List<Step>();
        String stpStr = valu.data["steps"];
        var temp = stpStr.split(";");
        temp.forEach((s) {
          steps.add(Step(
              null,
              null,
              null,
              null,
              null,
              Location(double.parse(s.split("|")[0].split(",")[0]),
                  double.parse(s.split("|")[0].split(",")[1])),
              Location(double.parse(s.split("|")[1].split(",")[0]),
                  double.parse(s.split("|")[1].split(",")[1])),
              null,
              null));
        });

        return ServiceValet(
            horaInicio: valu.data["horaInicio"],
            valet: Valet(
                nombre: valu.data["valet-nombre"],
                numero: valu.data["valet-numero"]),
            locationCar: valu.data["locationCar"],
            locationDestination: valu.data["locationDestination"],
            steps: steps,
            type: valu.data["type"]);
      } else {
        return ServiceParked(
            locationCar: valu.data["locationCar"],
            valet: Valet(
                nombre: valu.data["valet-nombre"],
                numero: valu.data["valet-numero"]),
            horaInicio: valu.data["horaInicio"]);
      }
    });
  }

  crearServicio(String correo, Service service) async {
    if (!connectionStatus.hasConnection) {
      throw ("Sin Conexión");
    }
    if (service is ServiceParked) {
      await Firestore.instance
          .collection('servicios')
          .document(correo)
          .setData({
        "horaInicio": service.horaInicio,
        "tipo": service.tipo,
        "valet-nombre": service.valet.nombre,
        "valet-numero": service.valet.numero,
        "locationCar": service.locationCar
      });
    } else if (service is ServiceValet) {
      String stpStr = "";
      service.steps.forEach((step) {
        stpStr += ";" +
            step.startLocation.toString() +
            "|" +
            step.endLocation.toString();
      });
      stpStr = stpStr.replaceFirst(";", "");
      await Firestore.instance
          .collection('servicios')
          .document(correo)
          .setData({
        "horaInicio": service.horaInicio,
        "tipo": service.tipo,
        "valet-nombre": service.valet.nombre,
        "valet-numero": service.valet.numero,
        "locationCar": service.locationCar,
        "locationDestination": service.locationDestination,
        "steps": stpStr,
        "type": service.type
      });
    }
  }

  borrarServicio(String correo) async {
    await Firestore.instance.collection('servicios').document(correo).delete();
  }

  Future<dynamic> createReserva(
      ParkingLot parkingLot, Reserva reserva, authState) async {
    if (!connectionStatus.hasConnection) {
      contador++;
      if (contador > 3) {
        throw ("Por favor Intente más Tarde!");
      }
    }
    print('ParkingLot id: ${parkingLot.id} ${parkingLot.address}');
    DocumentReference _ref =
        Firestore.instance.collection('services').document();
    await _ref.setData({
      'startHour': reserva.startHour,
      'endHour': reserva.endHour,
      'payment': 'credit',
      'reservation': true,
      'user': authState.user.email,
      'parking': parkingLot.id,
    });
    return _ref.documentID;
  }

  Future<dynamic> getPersonasFila(ParkingLot parkingLot) async {
    int contPuestos = 0;

    await Firestore.instance
        .collection('services')
        .getDocuments()
        .then((onValue) {
      var docs = onValue.documents;
      for (int i = 0; i < docs.length; i++) {
        var temp = docs[i].data;
        if (temp.containsKey('fila')) {
          if (temp['parking'] == parkingLot.id) {
            if (temp['enHour'] != null) {
              var comp = Timestamp.now().compareTo((temp['endHour']));
              if (comp > 0) {
                contPuestos++;
              }
            } else {
              contPuestos++;
            }
          }
        }
      }
    });
    return contPuestos;
  }

  Future<dynamic> createFila(ParkingLot parkingLot, authState) async {
    if (!connectionStatus.hasConnection) {
      contador++;
      if (contador > 3) {
        throw ("Por favor Intente más Tarde!");
      }
    }

    var _ref = await Firestore.instance.collection('services').document();
    _ref.setData({
      'fila': true,
      'parking': parkingLot.id,
      'user': authState.user.email,
      'startHour': DateTime.now(),
      'endHour': ""
    });
    return _ref.documentID;
  }

  Future<dynamic> salirFila(String idFila) async {
    if (!connectionStatus.hasConnection) {
      contador++;
      if (contador > 3) {
        throw ("Por favor Intente más Tarde! No hay conexión!");
      }
    }
    return await Firestore.instance
        .collection('services')
        .document(idFila)
        .updateData({'endHour': DateTime.now()});
  }

  void cancelarReserva(String idReserva) async {
    await Firestore.instance
        .collection('services')
        .document(idReserva)
        .updateData({'endHour': DateTime.now()});
  }
}
