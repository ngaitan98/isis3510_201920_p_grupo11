import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
//import 'file:///C:/programas/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_facebook_login-3.0.0/lib/flutter_facebook_login.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:google_sign_in/google_sign_in.dart';

class UserInfo {
  final String rol;
  final String email;
  final String gender;
  final int age;
  final String name;
  final String lastName;
  final String phone;
  UserInfo(
      {@required this.rol,
      @required this.email,
      this.gender,
      this.age,
      this.name,
      this.lastName,
      this.phone});

  @override
  String toString() =>
      "{ Rol: $rol, Email: $email, Name: $name, Lastname: $lastName, Gender: $gender, Age: $age, Phone: $phone }";
}

//Clase que se encarga de manejar la persistencia local de datos.
class UserRepository {
  // GoogleSignIn _googleSignIn = GoogleSignIn(
  //   scopes: <String>['email'],
  // );
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<UserInfo> authenticate(
      {@required String type, String email, String password}) async {
    if (type == "Email") {
      AuthResult result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      return await Firestore.instance
          .collection('users')
          .where("email", isEqualTo: result.user.email)
          .getDocuments()
          .then((QuerySnapshot docs) {
        return new UserInfo(
            rol: docs.documents[0].data["role"], email: result.user.email);
      });
    } else if (type == "Google") {
      // final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
      // final GoogleSignInAuthentication googleAuth =
      //     await googleUser.authentication;

      // final AuthCredential credential = GoogleAuthProvider.getCredential(
      //   accessToken: googleAuth.accessToken,
      //   idToken: googleAuth.idToken,
      // );

      // final FirebaseUser user =
      //     (await _auth.signInWithCredential(credential)).user;
      return null;
    } else {
      //FacebookLogin facebookLogin = FacebookLogin();
      // final result = await facebookLogin.logInWithReadPermissions(['email']);
      // if (result.status == FacebookLoginStatus.loggedIn) {
      //   return new UserInfo(rol: "", email: "");
      // } else if (result.status == FacebookLoginStatus.cancelledByUser) {
      //   return null;
      // } else if (result.status == FacebookLoginStatus.error) {
      //   throw (result.errorMessage);
      // } else {
      //   throw ("Error tipo Facebooklogin");
      // }
      return null;
    }
  }

  Future<void> logOut() async {
    final sharedPrefs = await SharedPreferences.getInstance();
    sharedPrefs.remove("rol");
    sharedPrefs.remove("email");
    _auth.signOut();
    return;
  }

  Future<void> persistUserInfo(UserInfo user) async {
    //Dependiendo de la información que se necesite se guarda aquí.
    final sharedPrefs = await SharedPreferences.getInstance();
    sharedPrefs.setString("rol", user.rol);
    sharedPrefs.setString("email", user.email);
    return;
  }

  Future<UserInfo> getUser() async {
    final sharedPrefs = await SharedPreferences.getInstance();
    try {
      return UserInfo(
          rol: sharedPrefs.getString("rol"),
          email: sharedPrefs.getString("email"));
    } catch (error) {
      logOut();
    }
    return null;
  }

  Future<bool> hasUser() async {
    final sharedPrefs = await SharedPreferences.getInstance();
    return sharedPrefs.containsKey("email");
  }

  register({@required UserInfo userInfo, @required String password}) async {
    await _auth.createUserWithEmailAndPassword(
        email: userInfo.email, password: password);
    await Firestore.instance
        .collection('users')
        .document(userInfo.email)
        .setData({
      'age': userInfo.age,
      'email': userInfo.email,
      'gender': userInfo.gender,
      'lastName': userInfo.lastName,
      'name': userInfo.name,
      'phone': userInfo.phone,
      'role': 'client'
    });
  }

  //Metodos del celular
  void call(String number) async {
    var launchStr = "tel:$number";
    if (await canLaunch(launchStr)) {
      await launch(launchStr);
    } else {
      throw 'Could not launch $launchStr';
    }
  }

  void sendSms(String number) async {
    var launchStr = "sms:$number";
    if (await canLaunch(launchStr)) {
      await launch(launchStr);
    } else {
      throw 'Could not launch $launchStr';
    }
  }

  void sendEmail(String email) async {
    var launchStr = "mailto:$email";
    if (await canLaunch(launchStr)) {
      await launch(launchStr);
    } else {
      throw 'Could not launch $launchStr';
    }
  }
}
