import 'package:meta/meta.dart';

@immutable
abstract class MyParkingLotsState {}

class MyParkingLotsLoadingState extends MyParkingLotsState {
  @override
  String toString() => 'MyParkingLotsLoadingState';
}

class MyParkingLotsFailureState extends MyParkingLotsState {
  final String error;
  MyParkingLotsFailureState({@required this.error});

  @override
  String toString() => 'MyParkingLotsFailureState { error: $error }';
}

class MyParkingLotsAddedState extends MyParkingLotsState {
  @override
  String toString() => 'MyParkingLotsAddedState';
}

class MyParkingLotsEditedState extends MyParkingLotsState {
  @override
  String toString() => 'MyParkingLotsEditedState';
}

class MyReserveDoneState extends MyParkingLotsState {
  @override
  String toString() => 'MyReserveDoneState';
}
