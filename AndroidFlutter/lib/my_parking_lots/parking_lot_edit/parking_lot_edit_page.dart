import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_repository/firebase_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qpark/common/regular_expresions.dart';
import 'package:qpark/my_parking_lots/my_parking_lots.dart';
import 'package:qpark/navigator/navigator.dart';

class ParkingLotEditPage extends StatelessWidget {
  ParkingLotEditPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ParkingLotEditForm(),
    );
  }
}

class ParkingLotEditArguments {
  final MyParkingLotsBloc bloc;
  final ParkingLot parkingLot;

  ParkingLotEditArguments({@required this.bloc, @required this.parkingLot});
}

class ParkingLotEditForm extends StatefulWidget {
  @override
  State<ParkingLotEditForm> createState() => _ParkingLotEditFormState();
}

class _ParkingLotEditFormState extends State<ParkingLotEditForm> {
  final _formKey = new GlobalKey<FormState>();
  TextEditingController _address = TextEditingController();
  TextEditingController _localidad = TextEditingController();
  int _costPerMinute;
  GeoPoint _location;
  TextEditingController _name = TextEditingController();
  int _zipcode;
  int _parkingSpots;

  @override
  Widget build(BuildContext context) {
    final NavigatorBloc navigatorBloc = BlocProvider.of<NavigatorBloc>(context);
    final ParkingLotEditArguments args =
        ModalRoute.of(context).settings.arguments;
    _location = args.parkingLot.location;
    return new Container(
        padding: EdgeInsets.all(16.0),
        child: new Form(
          key: _formKey,
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                child: new TextFormField(
                  maxLines: 1,
                  initialValue: args.parkingLot.name,
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  maxLength: 50,
                  decoration: new InputDecoration(
                      hintText: 'Nombre',
                      icon: new Icon(
                        Icons.local_parking,
                        color: Colors.grey,
                      )),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'El nombre no puede estar vacío';
                    } else if (!direccion.hasMatch(value.replaceAll(" ", ""))) {
                      return 'El valor ingresado no es válido!';
                    }
                    return null;
                  },
                  controller: _name,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                child: new TextFormField(
                  maxLines: 1,
                  initialValue: args.parkingLot.address,
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  maxLength: 100,
                  decoration: new InputDecoration(
                      hintText: 'Dirección',
                      icon: new Icon(
                        Icons.directions,
                        color: Colors.grey,
                      )),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'La direccion no puede estar vacía';
                    } else if (!direccion.hasMatch(value.replaceAll(" ", ""))) {
                      return 'El valor ingresado no es válido!';
                    }
                    return null;
                  },
                  controller: _address,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                child: new TextFormField(
                  maxLines: 1,
                  initialValue: args.parkingLot.localidad,
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  maxLength: 50,
                  decoration: new InputDecoration(
                      hintText: 'Localidad',
                      icon: new Icon(
                        Icons.location_city,
                        color: Colors.grey,
                      )),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'La localidad no puede estar vacía';
                    } else if (!alpha.hasMatch(value)) {
                      return 'El valor ingresado no es válido!';
                    }
                    return null;
                  },
                  controller: _localidad,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                child: new TextFormField(
                  maxLines: 1,
                  initialValue: args.parkingLot.zipCode.toString(),
                  keyboardType: TextInputType.number,
                  autofocus: false,
                  maxLength: 10,
                  decoration: new InputDecoration(
                      hintText: 'Codígo Postal',
                      icon: new Icon(
                        Icons.markunread_mailbox,
                        color: Colors.grey,
                      )),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'El Codígo Postal no puede estar vacío';
                    } else if (!numeric.hasMatch(value)) {
                      return 'El valor ingresado no es válido!';
                    }
                    return null;
                  },
                  onChanged: (value) => _zipcode = int.parse(value),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                child: new TextFormField(
                  maxLines: 1,
                  initialValue: args.parkingLot.costPerMinute.toString(),
                  keyboardType: TextInputType.number,
                  autofocus: false,
                  maxLength: 10,
                  decoration: new InputDecoration(
                      hintText: 'Costo por minuto',
                      icon: new Icon(
                        Icons.monetization_on,
                        color: Colors.grey,
                      )),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'El costo por minuto no puede estar vacío';
                    } else if (!numeric.hasMatch(value)) {
                      return 'El valor ingresado no es válido!';
                    }
                    return null;
                  },
                  onChanged: (value) => _costPerMinute = int.parse(value),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                child: new TextFormField(
                  maxLines: 1,
                  initialValue: args.parkingLot.parkingSpots.toString(),
                  keyboardType: TextInputType.number,
                  autofocus: false,
                  maxLength: 10,
                  decoration: new InputDecoration(
                      hintText: 'Cupos',
                      icon: new Icon(
                        Icons.directions_car,
                        color: Colors.grey,
                      )),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Los cupos no pueden estar vacíos';
                    } else if (!numeric.hasMatch(value)) {
                      return 'El valor ingresado no es válido!';
                    }
                    return null;
                  },
                  onChanged: (value) => _parkingSpots = int.parse(value),
                ),
              ),
              Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
                  child: SizedBox(
                    height: 40.0,
                    child: new RaisedButton(
                      elevation: 5.0,
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0)),
                      color: Colors.blue,
                      child: new Text('Guardar',
                          style: new TextStyle(
                              fontSize: 20.0, color: Colors.white)),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();
                          args.bloc.dispatch(EditParkingLotEvent(
                              parkingLot: new ParkingLot(
                                  name: _name.text,
                                  zipCode: _zipcode,
                                  parkingSpots: _parkingSpots,
                                  owner: args.parkingLot.owner,
                                  localidad: _localidad.text,
                                  costPerMinute: _costPerMinute,
                                  id: args.parkingLot.id,
                                  address: _address.text,
                                  location: _location)));
                          navigatorBloc.dispatch(NavigatorActionPop(null));
                        }
                      },
                    ),
                  ))
            ],
          ),
        ));
  }
}
