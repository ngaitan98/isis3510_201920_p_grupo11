import 'dart:async';
import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'my_parking_lots.dart';
import 'package:firebase_repository/firebase_repository.dart';
import 'package:user_repository/user_repository.dart';
import 'package:location/location.dart';

class MyParkingLotsBloc extends Bloc<MyParkingLotsEvent, MyParkingLotsState> {
  final FireBaseRepository firebaseRepository;
  final UserRepository userRepository;
  Location location = Location();

  MyParkingLotsBloc(
      {@required this.firebaseRepository, @required this.userRepository})
      : assert(firebaseRepository != null);

  @override
  MyParkingLotsState get initialState => MyParkingLotsLoadingState();
  @override
  Stream<MyParkingLotsState> mapEventToState(MyParkingLotsEvent event) async* {
    try {
      if (event is DeleteParkingLotEvent) {
        await firebaseRepository.deleteParkingLot(event.parkingLot);
      } else if (event is AddParkingLotEvent) {
        var permission = await location.requestPermission();
        if (permission) {
          var temp = await location.getLocation();
          event.parkingLot.location = GeoPoint(temp.latitude, temp.longitude);
        } else {
          Random r = new Random();
          event.parkingLot.location = GeoPoint(
              4.6 + (4.711 - 4.6) * r.nextDouble(),
              -74.06 + (-74 + 74.06) * r.nextDouble());
        }
        await firebaseRepository.addParkingLot(event.parkingLot);
        yield MyParkingLotsAddedState();
      } else if (event is EditParkingLotEvent) {
        await firebaseRepository.editParkingLot(event.parkingLot);
        yield MyParkingLotsEditedState();
      } else if (event is MyParkingLotFailureEvent) {
        yield MyParkingLotsFailureState(error: event.error);
      }
    } catch (error) {
      yield MyParkingLotsFailureState(error: error.toString());
    }
  }
}
