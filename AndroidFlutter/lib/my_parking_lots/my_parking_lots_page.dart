import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qpark/authentication/authentication.dart';
import 'package:qpark/navigator/navigator.dart';
import 'package:firebase_repository/firebase_repository.dart';
import 'package:user_repository/user_repository.dart';

import 'my_parking_lots.dart';

class MyParkingLots extends StatefulWidget {
  @override
  _MyParkingLotsState createState() => _MyParkingLotsState();
}

class _MyParkingLotsState extends State<MyParkingLots> {
  ScaffoldFeatureController _snack;

  @override
  Widget build(BuildContext context) {
    final NavigatorBloc navigatorBloc = BlocProvider.of<NavigatorBloc>(context);
    final AuthenticationBloc authenticationBloc =
        BlocProvider.of<AuthenticationBloc>(context);
    final AuthenticationAuthenticated authState =
        authenticationBloc.currentState is AuthenticationAuthenticated
            ? authenticationBloc.currentState
            : null;
    final FireBaseRepository firebaseRepository =
        RepositoryProvider.of<FireBaseRepository>(context);
    final UserRepository userRepository =
        RepositoryProvider.of<UserRepository>(context);

    final MyParkingLotsBloc myParkingLotsBloc = MyParkingLotsBloc(
      firebaseRepository: firebaseRepository,
      userRepository: userRepository,
    );
    return Scaffold(
        appBar: AppBar(
          title: Text('Mis Parqueaderos'),
          centerTitle: true,
        ),
        body: BlocProvider(
          builder: (context) {
            return myParkingLotsBloc;
          },
          child: Container(
            child: BlocListener<MyParkingLotsBloc, MyParkingLotsState>(
              listener: (context, state) {
                try {
                  _snack.close();
                } catch (error) {}
                if (state is MyParkingLotsFailureState) {
                  _snack = Scaffold.of(context).showSnackBar(
                    SnackBar(
                      content: Text('${state.error}'),
                      backgroundColor: Colors.red,
                    ),
                  );
                } else if (state is MyParkingLotsAddedState) {
                  _snack = Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text("Parqueadero Agregado"),
                    backgroundColor: Colors.green,
                  ));
                } else if (state is MyParkingLotsEditedState) {
                  _snack = Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text("Parqueadero Editado"),
                    backgroundColor: Colors.blue,
                  ));
                }
              },
              child: StreamBuilder<QuerySnapshot>(
                  stream: Firestore.instance
                      .collection('parkings')
                      .where("owner", isEqualTo: authState.user.email)
                      .snapshots(),
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (!snapshot.hasData) {
                      return Container(
                          child: Center(
                        child: CircularProgressIndicator(),
                      ));
                    }
                    return ListView.builder(
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) {
                        final DocumentSnapshot valu =
                            snapshot.data.documents[index];
                        final ParkingLot parkingLot = ParkingLot(
                            id: valu.documentID,
                            name: valu.data["name"],
                            address: valu.data["address"],
                            costPerMinute: valu.data["costPerMinute"],
                            location: valu.data["location"],
                            localidad: valu.data["localidad"],
                            owner: valu.data["owner"],
                            parkingSpots: valu.data["parkingSpots"],
                            zipCode: valu.data["zipCode"]);
                        return Dismissible(
                          key: ObjectKey(parkingLot),
                          child: Card(
                            child: ListTile(
                              leading: Icon(Icons.local_parking),
                              title: Text(parkingLot.name),
                              onTap: () {
                                navigatorBloc.dispatch(
                                    NavigateToParkingLotDetail(
                                        parkingLot: parkingLot));
                              },
                              onLongPress: () {
                                navigatorBloc.dispatch(NavigateToEditParkingLot(
                                    parkingLot: parkingLot,
                                    bloc: myParkingLotsBloc));
                              },
                            ),
                          ),
                          confirmDismiss:
                              (DismissDirection dismissDirection) async {
                            if (dismissDirection ==
                                DismissDirection.endToStart) {
                              return await showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text("Borrar"),
                                    content: Text(
                                        "Está seguro que quiere borrar el Parqueadero: " +
                                            parkingLot.name),
                                    actions: [
                                      FlatButton(
                                        child: Text("Cancelar"),
                                        onPressed: () {
                                          navigatorBloc.dispatch(
                                              NavigatorActionPop(false));
                                        },
                                      ),
                                      FlatButton(
                                        child: Text("Borrar"),
                                        onPressed: () {
                                          if (firebaseRepository.contador > 3) {
                                            myParkingLotsBloc.dispatch(
                                                (MyParkingLotFailureEvent(
                                                    error:
                                                        "Por favor Intente más Tarde!")));
                                            navigatorBloc.dispatch(
                                                NavigatorActionPop(false));
                                          } else {
                                            navigatorBloc.dispatch(
                                                NavigatorActionPop(true));
                                          }
                                        },
                                      ),
                                    ],
                                  );
                                },
                              );
                            } else {
                              return false;
                            }
                          },
                          onDismissed: (direction) {
                            myParkingLotsBloc.dispatch(
                                DeleteParkingLotEvent(parkingLot: parkingLot));
                          },
                          background: Container(
                            alignment: Alignment.centerRight,
                            padding: EdgeInsets.only(right: 20.0),
                            color: Colors.red,
                            child: Icon(
                              Icons.delete,
                              color: Colors.white,
                            ),
                          ),
                        );
                      },
                    );
                  }),
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            navigatorBloc.dispatch(NavigateToAddParkingLot(myParkingLotsBloc));
          },
          child: Icon(Icons.add),
        ));
  }
}
