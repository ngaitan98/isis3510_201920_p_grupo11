import 'package:meta/meta.dart';
import 'package:firebase_repository/firebase_repository.dart';

abstract class MyParkingLotsEvent {
  MyParkingLotsEvent([List props = const []]) : super();
}

class DeleteParkingLotEvent extends MyParkingLotsEvent {
  final ParkingLot parkingLot;
  DeleteParkingLotEvent({@required this.parkingLot});

  @override
  String toString() => 'DeleteParkingLotEvent { ParkingLot: $parkingLot }';
}

class AddParkingLotEvent extends MyParkingLotsEvent {
  final ParkingLot parkingLot;
  AddParkingLotEvent({@required this.parkingLot});
  @override
  String toString() => 'AddParkingLotEvent { ParkingLot: $parkingLot }';
}

class EditParkingLotEvent extends MyParkingLotsEvent {
  final ParkingLot parkingLot;
  EditParkingLotEvent({@required this.parkingLot});
  @override
  String toString() => 'EditParkingLotEvent { ParkingLot: $parkingLot }';
}

class ReserveParkingLotEvent extends MyParkingLotsEvent {
  final ParkingLot parkingLot;
  final Reserva reserva;
  ReserveParkingLotEvent({@required this.parkingLot, @required this.reserva});
  @override
  String toString() {
    return 'ReserveParkingLotEvent { ParkingLot: $parkingLot }';
  }
}

class MyParkingLotFailureEvent extends MyParkingLotsEvent {
  final String error;
  MyParkingLotFailureEvent({@required this.error});
  @override
  String toString() {
    return 'MyParkingLotFailureEvent { Error: $error }';
  }
}
