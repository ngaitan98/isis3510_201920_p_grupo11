import 'package:firebase_repository/firebase_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qpark/authentication/authentication.dart';
import 'package:qpark/common/regular_expresions.dart';
import 'package:qpark/my_parking_lots/my_parking_lots.dart';
import 'package:qpark/navigator/navigator.dart';

class ParkingLotAddPage extends StatelessWidget {
  ParkingLotAddPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Create A ParkingLot'),
      ),
      body: ParkingLotAddForm(),
    );
  }
}

class ParkingLotAddForm extends StatefulWidget {
  @override
  State<ParkingLotAddForm> createState() => _ParkingLotAddFormState();
}

class _ParkingLotAddFormState extends State<ParkingLotAddForm> {
  final _formKey = new GlobalKey<FormState>();
  TextEditingController _address = TextEditingController();
  TextEditingController _localidad = TextEditingController();
  TextEditingController _name = TextEditingController();
  int _zipcode;
  int _parkingSpots;
  int _costPerMinute;
  // String _location;

  @override
  Widget build(BuildContext context) {
    final NavigatorBloc navigatorBloc = BlocProvider.of<NavigatorBloc>(context);
    final AuthenticationBloc authenticationBloc =
        BlocProvider.of<AuthenticationBloc>(context);
    final MyParkingLotsBloc bloc = ModalRoute.of(context).settings.arguments;
    return new Container(
        padding: EdgeInsets.all(16.0),
        child: new Form(
          key: _formKey,
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                child: new TextFormField(
                  maxLines: 1,
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  maxLength: 50,
                  controller: _name,
                  decoration: new InputDecoration(
                      hintText: 'Nombre',
                      icon: new Icon(
                        Icons.local_parking,
                        color: Colors.grey,
                      )),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'El nombre no puede estar vacío';
                    } else if (!direccion.hasMatch(value.replaceAll(" ", ""))) {
                      return 'El valor ingresado no es válido!';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                child: new TextFormField(
                  maxLines: 1,
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  maxLength: 100,
                  decoration: new InputDecoration(
                      hintText: 'Dirección',
                      icon: new Icon(
                        Icons.directions,
                        color: Colors.grey,
                      )),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'La dirección no puede estar vacía';
                    } else if (!direccion.hasMatch(value.replaceAll(" ", ""))) {
                      return 'El valor ingresado no es válido!';
                    }
                    return null;
                  },
                  controller: _address,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                child: new TextFormField(
                  maxLines: 1,
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  maxLength: 50,
                  decoration: new InputDecoration(
                      hintText: 'Localidad',
                      icon: new Icon(
                        Icons.location_city,
                        color: Colors.grey,
                      )),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'La localidad no puede estar vacía';
                    } else if (!alpha.hasMatch(value)) {
                      return 'El valor ingresado no es válido!';
                    }
                    return null;
                  },
                  controller: _localidad,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                child: new TextFormField(
                  maxLines: 1,
                  keyboardType: TextInputType.number,
                  autofocus: false,
                  maxLength: 10,
                  decoration: new InputDecoration(
                      hintText: 'Código Postal',
                      icon: new Icon(
                        Icons.markunread_mailbox,
                        color: Colors.grey,
                      )),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'El Código Postal no puede estar vacío';
                    } else if (!numeric.hasMatch(value)) {
                      return 'El valor ingresado no es válido!';
                    }
                    return null;
                  },
                  onChanged: (value) => _zipcode = int.parse(value),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                child: new TextFormField(
                  maxLines: 1,
                  keyboardType: TextInputType.number,
                  autofocus: false,
                  maxLength: 10,
                  decoration: new InputDecoration(
                      hintText: 'Costo por minuto',
                      icon: new Icon(
                        Icons.monetization_on,
                        color: Colors.grey,
                      )),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'El costo por minuto no puede estar vacío';
                    } else if (!numeric.hasMatch(value)) {
                      return 'El valor ingresado no es válido!';
                    }
                    return null;
                  },
                  onChanged: (value) => _costPerMinute = int.parse(value),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                child: new TextFormField(
                  maxLines: 1,
                  keyboardType: TextInputType.number,
                  autofocus: false,
                  maxLength: 10,
                  decoration: new InputDecoration(
                      hintText: 'Cupos',
                      icon: new Icon(
                        Icons.directions_car,
                        color: Colors.grey,
                      )),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Los cupos no pueden estar vacíos';
                    } else if (!numeric.hasMatch(value)) {
                      return 'El valor ingresado no es válido!';
                    }
                    return null;
                  },
                  onChanged: (value) => _parkingSpots = int.parse(value),
                ),
              ),
              Padding(
                  padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
                  child: SizedBox(
                    height: 40.0,
                    child: new RaisedButton(
                      elevation: 5.0,
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0)),
                      color: Colors.blue,
                      child: new Text('Crear el parqueadero',
                          style: new TextStyle(
                              fontSize: 20.0, color: Colors.white)),
                      onPressed: () {
                        AuthenticationState state =
                            authenticationBloc.currentState;
                        if (_formKey.currentState.validate() &&
                            state is AuthenticationAuthenticated) {
                          _formKey.currentState.save();
                          try {
                            bloc.dispatch(AddParkingLotEvent(
                                parkingLot: new ParkingLot(
                                    name: _name.text,
                                    zipCode: _zipcode,
                                    parkingSpots: _parkingSpots,
                                    owner: state.user.email,
                                    localidad: _localidad.text,
                                    costPerMinute: _costPerMinute,
                                    id: null,
                                    address: _address.text,
                                    location: null)));
                          } catch (error) {
                            print(error);
                          }
                          navigatorBloc.dispatch(NavigatorActionPop(null));
                        }
                      },
                    ),
                  ))
            ],
          ),
        ));
  }
}
