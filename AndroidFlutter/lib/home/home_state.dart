abstract class HomeState {
  const HomeState();
}

class HomeInitial extends HomeState {
  @override
  String toString() => 'HomeInitial';
}

class HomeLoading extends HomeState {}

class HomeError extends HomeState {
  final String strErr;

  HomeError(this.strErr);

  @override
  String toString() => 'HomeError';
}

class SearchError extends HomeState {
  final String strErr;

  SearchError(this.strErr);

  @override
  String toString() => 'SearchError';
}

class HomeReconnected extends HomeState {
  @override
  String toString() => 'HomeReconnected';
}
