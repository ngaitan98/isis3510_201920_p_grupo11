import 'package:flutter/foundation.dart';

@immutable
abstract class HomeEvent {}

class InitialEvent extends HomeEvent {
  final bool permission;

  InitialEvent(this.permission);

  @override
  String toString() => 'InitialEvent';
}

class ConnectionError extends HomeEvent {
  final String strErr;

  ConnectionError(this.strErr);

  @override
  String toString() => 'ConnectionError';
}

class SearchErrorEvent extends HomeEvent {
  final String strErr;

  SearchErrorEvent(this.strErr);

  @override
  String toString() => 'SearchErrorEvent';
}

class CheckConnection extends HomeEvent {}

class ConnectionReconnected extends HomeEvent {}
