import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:connection_status/connection_status.dart';

import 'home.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  StreamSubscription _connectionChangeStream;
  ConnectionStatus connectionStatus = ConnectionStatus.getInstance();
  bool isOffline = false;

  @override
  HomeState get initialState => HomeInitial();

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is InitialEvent) {
      _connectionChangeStream =
          connectionStatus.connectionChange.listen(connectionChanged);
      _connectionChangeStream.onError((error) {
        print("Error de home_connection_changed, {Error: $error } ");
      });
    } else if (event is ConnectionError) {
      yield HomeError(event.strErr);
    } else if (event is ConnectionReconnected) {
      yield HomeReconnected();
    } else if (event is SearchErrorEvent) {
      yield SearchError(event.strErr);
    } else if (event is CheckConnection) {
      if (!connectionStatus.hasConnection) {
        dispatch(ConnectionError("No hay Conexión"));
      }
    }
  }

  void connectionChanged(dynamic hasConnection) {
    if (!hasConnection) {
      dispatch(ConnectionError("No hay Conexión"));
    } else {
      dispatch(ConnectionReconnected());
    }
  }
}
