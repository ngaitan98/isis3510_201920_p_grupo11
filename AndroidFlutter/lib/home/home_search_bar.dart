import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connection_status/connection_status.dart';
import 'package:flutter/material.dart';
import 'package:qpark/home/home.dart';
import 'package:qpark/map/camera_bloc/camera.dart';
import 'package:qpark/map/map.dart';
import 'package:qpark/navigator/navigator.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_webservice/places.dart';

import '../main.dart';

class SearchList extends StatefulWidget {
  SearchList({Key key}) : super(key: key);
  @override
  _SearchListState createState() => new _SearchListState();
}

class _SearchListState extends State<SearchList> {
  Widget appBarTitle = new Text(
    "Buscar",
    style: new TextStyle(color: Colors.white),
  );
  Icon actionIcon = new Icon(
    Icons.search,
    color: Colors.white,
  );
  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);
  final key = new GlobalKey<ScaffoldState>();
  List<InfoLocation> _list;
  String _searchText = "";
  ScaffoldFeatureController _snack;

  @override
  void initState() {
    super.initState();
    init();
  }

  void init() {
    _list = List();
  }

  @override
  Widget build(BuildContext context) {
    final HomeBloc homeBloc = BlocProvider.of<HomeBloc>(context);
    ConnectionStatus connectionStatus = ConnectionStatus.getInstance();
    return Scaffold(
        key: key,
        primary: true,
        appBar: AppBar(
            title: Center(
                child: Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25.0),
                          color: Colors.white),
                      child: TextField(
                        autofocus: true,
                        maxLines: 1,
                        maxLength: 50,
                        onChanged: (value) {
                          _searchText = value;
                          if (connectionStatus.hasConnection) {
                            _places.searchByText(_searchText).then((resp) {
                              if (resp.results != null) {
                                List<InfoLocation> lista = List();
                                resp.results.forEach((res) {
                                  var info = new InfoLocation(
                                      new GeoPoint(res.geometry.location.lat,
                                          res.geometry.location.lng),
                                      res.name);
                                  lista.add(info);
                                });
                                try {
                                  setState(() {
                                    _list = lista;
                                  });
                                } catch (error) {}
                              } else {
                                homeBloc.dispatch(
                                    SearchErrorEvent("No hay Conexión"));
                              }
                            });
                          } else {
                            homeBloc
                                .dispatch(SearchErrorEvent("No hay Conexión"));
                          }
                        },
                        onSubmitted: (text) {
                          if (_list.length > 0) {
                            BlocProvider.of<MapCameraBloc>(context).dispatch(
                                MoveToEvent(location: _list[0].location));
                            BlocProvider.of<NavigatorBloc>(context)
                                .dispatch(NavigatorActionPop(false));
                          }
                        },
                        decoration: InputDecoration(
                            hintText: "Search",
                            counterText: "",
                            contentPadding: EdgeInsets.only(left: 25),
                            suffixIcon: IconButton(
                              icon: Icon(Icons.search),
                              iconSize: 30.0,
                              onPressed: () {},
                            ),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(25.0))),
                      ),
                    )))),
        body: BlocListener<HomeBloc, HomeState>(
            listener: (context, state) {
              try {
                _snack.close();
              } catch (error) {}
              if (state is SearchError) {
                _snack = Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text(state.strErr),
                  backgroundColor: Colors.red,
                  duration: Duration(days: 1),
                ));
              }
            },
            child: new ListView(
              padding: new EdgeInsets.symmetric(vertical: 8.0),
              children: _buildSearchList(),
            )));
  }

  List<ChildItem> _buildSearchList() {
    if (_searchText.isEmpty) {
      return _list.map((info) => new ChildItem(info)).toList();
    } else {
      return _list.map((info) => new ChildItem(info)).toList();
    }
  }
}

class ChildItem extends StatelessWidget {
  final InfoLocation info;
  ChildItem(this.info);
  @override
  Widget build(BuildContext context) {
    return new ListTile(
      title: new Text(this.info.name),
      onTap: () {
        BlocProvider.of<MapCameraBloc>(context)
            .dispatch(MoveToEvent(location: info.location));
        BlocProvider.of<NavigatorBloc>(context)
            .dispatch(NavigatorActionPop(false));
      },
    );
  }
}

class InfoLocation {
  final GeoPoint location;
  final String name;
  InfoLocation(this.location, this.name);
}
