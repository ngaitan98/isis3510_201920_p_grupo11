import 'package:flutter/material.dart';
import 'package:qpark/ServiceManager.dart';
import 'package:qpark/navigator/navigator.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qpark/authentication/authentication.dart';
import 'package:qpark/map/map.dart';
import 'home.dart';

class HomePage extends StatefulWidget {
  final ServiceManager serviceManager;
  HomePage(this.serviceManager);
  @override
  _HomePageState createState() => _HomePageState(serviceManager);
}

class _HomePageState extends State<HomePage> {
  final ServiceManager serviceManager;
  _HomePageState(this.serviceManager);
  ScaffoldFeatureController _snack;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final AuthenticationBloc authenticationBloc =
        BlocProvider.of<AuthenticationBloc>(context);

    final NavigatorBloc navigatorBloc = BlocProvider.of<NavigatorBloc>(context);
    return Scaffold(
        key: _scaffoldKey,
        drawer: Drawer(child:
            BlocBuilder<AuthenticationBloc, AuthenticationState>(
                builder: (context, state) {
          List<Widget> children = [];
          _addDefaultChildren(context, state, children);
          if (state is AuthenticationAuthenticated) {
            _addRoleChildren(
                state, context, children, authenticationBloc, navigatorBloc);
          }
          return ListView(padding: EdgeInsets.zero, children: children);
        })),
        body: BlocListener<HomeBloc, HomeState>(listener: (context, state) {
          try {
            _snack.close();
          } catch (error) {}
          if (state is HomeError) {
            _snack = Scaffold.of(context).showSnackBar(SnackBar(
              content: Text(state.strErr),
              backgroundColor: Colors.red,
              duration: Duration(days: 1),
            ));
          } else if (state is HomeReconnected) {
            _snack = Scaffold.of(context).showSnackBar(SnackBar(
              content: Text("Reconectado"),
              backgroundColor: Colors.green,
              duration: Duration(seconds: 2),
            ));
          }
        }, child: BlocBuilder<MapsBloc, MapsState>(
          builder: (context, state) {
            var children = [
              InitialMap(
                serviceManager: serviceManager,
                mapsBloc: BlocProvider.of<MapsBloc>(context),
                homeBloc: BlocProvider.of<HomeBloc>(context),
              ),
              Align(
                  alignment: Alignment.topLeft,
                  child: SizedBox(
                    height: 100,
                    child: IconButton(
                      icon: Icon(Icons.menu),
                      onPressed: () => {_scaffoldKey.currentState.openDrawer()},
                      iconSize: 50,
                    ),
                  ))
            ];
            if (state is MapLoaderState) {
              children.add(Opacity(
                opacity: 0.3,
                child:
                    const ModalBarrier(dismissible: false, color: Colors.grey),
              ));
              if (state is MapSearchingState) {
                children.add(Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25.0),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            blurRadius: 5.0,
                            offset: new Offset(5.0, 5.0),
                          )
                        ],
                        color: Colors.white),
                    margin: const EdgeInsets.only(
                        left: 50.0, right: 50.0, top: 180, bottom: 180),
                    child: Center(
                        child: Column(children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(
                            top: 20.0,
                          ),
                          child: Text("Buscando tu Valet",
                              style: new TextStyle(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black))),
                      Expanded(
                          child: Container(
                              child: Center(
                                  child: SizedBox(
                                      height: 60.0,
                                      width: 60.0,
                                      child: CircularProgressIndicator())))),
                      RaisedButton(
                        elevation: 5.0,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        color: Theme.of(context).primaryColor,
                        child: new Text('Simular',
                            style: new TextStyle(
                                fontSize: 20.0, color: Colors.white)),
                        onPressed: () {
                          serviceManager.simulate(state.location);
                        },
                      ),
                      Padding(
                          padding: EdgeInsets.only(top: 8.0, bottom: 20),
                          child: RaisedButton(
                            elevation: 5.0,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0)),
                            color: Colors.red,
                            child: new Text('Cancelar',
                                style: new TextStyle(
                                    fontSize: 20.0, color: Colors.white)),
                            onPressed: () {
                              serviceManager.cancelar();
                            },
                          ))
                    ]))));
              } else if (state is MapWaitingState) {}
            }
            return Stack(children: children);
          },
        )));
  }

  _addDefaultChildren(context, state, children) {
    children.add(Container(
      height: 125.0,
      child: DrawerHeader(
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              child: Center(
                child: Text(state.user.email,
                    style: TextStyle(fontSize: 20, color: Colors.white)),
              ),
            ),
          ),
          decoration: BoxDecoration(color: Color.fromRGBO(31, 31, 54, 1)),
          margin: EdgeInsets.all(0.0),
          padding: EdgeInsets.all(0.0)),
    ));
    children.add(ListTile(
      title: Text('Valet'),
      onTap: () {
        serviceManager.createValet("MapParkingLotsState");
      },
    ));
    children.add(Divider(
      thickness: 2.0,
    ));
  }

  _addRoleChildren(
      state, context, children, authenticationBloc, navigatorBloc) {
    final rol = state.user.rol;
    if (rol == "Admin") {
      children.add(ListTile(
        title: Text('ADMIN'),
        onTap: () {
          Navigator.pop(context);
        },
      ));
    } else if (rol == "client") {
      children.add(ListTile(
        title: Text('Mis Parqueadas'),
        onTap: () {
          navigatorBloc.dispatch(NavigateToMyParkingHistory());
        },
      ));
    } else if (rol == "owner") {
      children.add(ListTile(
        title: Text('Mis Parqueaderos'),
        onTap: () {
          navigatorBloc.dispatch(NavigateToMyParkingLots());
        },
      ));
    }

    if (rol == "guest") {
      children.add(ListTile(
        title: Text('Ingresar'),
        onTap: () {
          authenticationBloc.dispatch(LoggedOut());
        },
      ));
    } else {
      children.add(Divider(
        thickness: 2.0,
      ));
      children.add(RaisedButton(
        child: Text('Desconéctate'),
        color: Theme.of(context).errorColor,
        onPressed: () {
          serviceManager.parar();
          authenticationBloc.dispatch(LoggedOut());
        },
      ));
    }
  }
}
