import 'package:qpark/map/map.dart';
import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_repository/firebase_repository.dart';
import 'package:location/location.dart' as LocationService;
import 'dart:math';
import 'package:meta/meta.dart';
import 'package:google_maps_webservice/directions.dart';
import 'package:user_repository/user_repository.dart';
import 'main.dart';

class ServiceManager {
  final MapsBloc mapsBloc;
  final FireBaseRepository fireBaseRepository;
  final UserRepository userRepository;
  LocationService.Location location = LocationService.Location();
  final directions = GoogleMapsDirections(apiKey: kGoogleApiKey);
  bool trabajando;
  String servicoActual;
  ServiceManager(
      {@required this.mapsBloc,
      @required this.fireBaseRepository,
      @required this.userRepository});

  cancelar() async {
    //Devolverse al estado anterior
    trabajando = false;
    await save(3, null);
    mapsBloc.dispatch(LoadViewEvent(service: null));
  }

  parar() async {
    trabajando = false;
  }

  createValet(String currentState) async {
    trabajando = false;
    try {
      if (!trabajando) {
        trabajando = true;
        var permission = await location.requestPermission();
        if (permission) {
          var temp = await location.getLocation();
          mapsBloc.dispatch(MapLoaderEvent(
              location: GeoPoint(temp.latitude, temp.longitude), type: 1));
        }
      }
    } catch (error) {
      print(error);
      trabajando = false;
    }
  }

  simulate(GeoPoint userGeo) async {
    Location userlocation = Location(userGeo.latitude, userGeo.longitude);
    if (trabajando) {
      //Busco un valet aleatorio.
      final rnd = new Random();
      var dif = rnd.nextDouble();
      var dif2 = rnd.nextDouble();
      var lat = rnd.nextBool()
          ? userlocation.lat + dif * 0.005
          : userlocation.lat - dif * 0.005;
      var lng = rnd.nextBool()
          ? userlocation.lng + dif2 * 0.005
          : userlocation.lng - dif2 * 0.005;
      var valetlocation = Location(lat, lng);
      var steps = List<Step>();
      DirectionsResponse res = await directions.directionsWithLocation(
          valetlocation, userlocation,
          travelMode: stringToTravelMode("walking"));
      if (res.isOkay) {
        if (res.routes.length > 0) {
          var legs = res.routes[0].legs;
          legs.forEach((l) {
            l.steps.forEach((s) {
              steps.add(s);
            });
          });
        }
        if (steps.length > 0) {
          var hora = DateTime.now();
          var service = ServiceValet(
              locationCar: GeoPoint(
                  steps[0].startLocation.lat, steps[0].startLocation.lng),
              locationDestination: GeoPoint(userlocation.lat, userlocation.lng),
              steps: steps,
              horaInicio: hora.hour.toString() + ":" + hora.minute.toString(),
              valet: new Valet(nombre: "NOMBRE", numero: "123456789"),
              type: "COMMING");
          await save(0, service);
          mapsBloc.dispatch(UpdateValetEvent(service: service));
          _simulation(service);
        } else {
          trabajando = false;
        }
      } else {
        print(res.errorMessage);
        trabajando = false;
      }
    }
  }

  simulateBringBack(
      GeoPoint locationCar, String horaInicio, Valet valet) async {
    try {
      if (!trabajando) {
        trabajando = true;
        var permission = await location.requestPermission();
        if (permission) {
          var temp = await location.getLocation();
          Location userlocation = Location(temp.latitude, temp.longitude);
          var steps = List<Step>();
          DirectionsResponse res = await directions.directionsWithLocation(
              Location(locationCar.latitude, locationCar.longitude),
              userlocation);
          if (res.isOkay) {
            if (res.routes.length > 0) {
              var legs = res.routes[0].legs;
              legs.forEach((l) {
                l.steps.forEach((s) {
                  steps.add(s);
                });
              });
            }
            if (steps.length > 0) {
              var service = ServiceValet(
                  horaInicio: horaInicio,
                  locationCar: GeoPoint(
                      steps[0].startLocation.lat, steps[0].startLocation.lng),
                  locationDestination:
                      GeoPoint(userlocation.lat, userlocation.lng),
                  steps: steps,
                  valet: valet,
                  type: "RETURNING");
              await save(3, null);
              mapsBloc.dispatch(UpdateValetEvent(service: service));
              _simulation(service);
            } else {
              trabajando = false;
            }
          } else {
            print(res.errorMessage);
            trabajando = false;
          }
        }
      }
    } catch (error) {
      print(error);
      trabajando = false;
    }
  }

  save(num, Service service) async {
    UserInfo usuario = await userRepository.getUser();
    if (num == 0 && service is ServiceValet) {
      Location userlocation = Location(service.locationDestination.latitude,
          service.locationDestination.longitude);
      final rnd = new Random();
      var dif = rnd.nextDouble();
      var dif2 = rnd.nextDouble();
      var lat = rnd.nextBool()
          ? userlocation.lat + dif * 0.01
          : userlocation.lat - dif * 0.01;
      var lng = rnd.nextBool()
          ? userlocation.lng + dif2 * 0.01
          : userlocation.lng - dif2 * 0.01;
      var parkingLotLocation = Location(lat, lng);
      var steps = List<Step>();
      DirectionsResponse res = await directions.directionsWithLocation(
          userlocation, parkingLotLocation);
      if (res.isOkay) {
        if (res.routes.length > 0) {
          var legs = res.routes[0].legs;
          legs.forEach((l) {
            l.steps.forEach((s) {
              steps.add(s);
            });
          });
        }
        if (steps.length > 0) {
          var newService = ServiceValet(
              horaInicio: service.horaInicio,
              valet: service.valet,
              locationCar: GeoPoint(
                  steps[0].startLocation.lat, steps[0].startLocation.lng),
              locationDestination:
                  GeoPoint(parkingLotLocation.lat, parkingLotLocation.lng),
              steps: steps,
              type: "PARKING");
          await fireBaseRepository.crearServicio(usuario.email, newService);
        }
      } else {
        print(res.errorMessage);
        trabajando = false;
      }
    } else if (num == 1 && service is ServiceValet) {
      var newService = ServiceParked(
          locationCar: service.locationDestination,
          horaInicio: service.horaInicio,
          valet: service.valet);
      await fireBaseRepository.crearServicio(usuario.email, newService);
    } else if (num == 3 && service == null) {
      await fireBaseRepository.borrarServicio(usuario.email);
    }
  }

  restartSim(ServiceValet service) async {
    if (service.type == "PARKING") {
      save(1, service);
    }
    trabajando = true;
    _simulation(service);
  }

  _simulation(ServiceValet service) async {
    if (trabajando) {
      await Future.delayed(Duration(seconds: 1));
      List<Step> steps = service.steps;
      Step step = steps[0];
      GeoPoint newLocation = await updateLocation(step);
      double distActual = await distance(
          steps[0].startLocation.lat,
          steps[0].startLocation.lng,
          newLocation.latitude,
          newLocation.longitude);
      double distTot = await distance(step.startLocation.lat,
          step.startLocation.lng, step.endLocation.lat, step.endLocation.lng);
      if (distActual > distTot) {
        newLocation = GeoPoint(step.endLocation.lat, step.endLocation.lng);
        steps.removeAt(0);
      } else {
        steps[0] = Step(
            step.travelMode,
            step.htmlInstructions,
            step.maneuver,
            step.polyline,
            step.transitDetails,
            Location(newLocation.latitude, newLocation.longitude),
            step.endLocation,
            step.duration,
            step.distance);
      }
      if (trabajando) {
        mapsBloc.dispatch(UpdateValetEvent(
            service: ServiceValet(
                horaInicio: service.horaInicio,
                valet: service.valet,
                locationCar: newLocation,
                locationDestination: service.locationDestination,
                steps: steps,
                type: service.type)));
      }
      if (steps.isEmpty) {
        if (service.type == "COMMING") {
          Location userlocation = Location(service.locationDestination.latitude,
              service.locationDestination.longitude);
          final rnd = new Random();
          var dif = rnd.nextDouble();
          var dif2 = rnd.nextDouble();
          var lat = rnd.nextBool()
              ? userlocation.lat + dif * 0.01
              : userlocation.lat - dif * 0.01;
          var lng = rnd.nextBool()
              ? userlocation.lng + dif2 * 0.01
              : userlocation.lng - dif2 * 0.01;
          var parkingLotLocation = Location(lat, lng);
          var steps = List<Step>();
          DirectionsResponse res = await directions.directionsWithLocation(
              userlocation, parkingLotLocation);
          if (res.isOkay) {
            if (res.routes.length > 0) {
              var legs = res.routes[0].legs;
              legs.forEach((l) {
                l.steps.forEach((s) {
                  steps.add(s);
                });
              });
            }
            if (steps.length > 0) {
              var newService = ServiceValet(
                  horaInicio: service.horaInicio,
                  valet: service.valet,
                  locationCar: GeoPoint(
                      steps[0].startLocation.lat, steps[0].startLocation.lng),
                  locationDestination:
                      GeoPoint(parkingLotLocation.lat, parkingLotLocation.lng),
                  steps: steps,
                  type: "PARKING");
              await save(1, newService);
              mapsBloc.dispatch(UpdateValetEvent(service: newService));
              _simulation(newService);
            } else {
              trabajando = false;
            }
          } else {
            print(res.errorMessage);
            trabajando = false;
          }
        } else if (service.type == "PARKING") {
          var service2 = ServiceParked(
              locationCar: service.locationDestination,
              horaInicio: service.horaInicio,
              valet: service.valet);
          mapsBloc.dispatch(LoadViewEvent(service: service2));
          trabajando = false;
        } else if (service.type == "RETURNING") {
          //Que mostrar despues?
          trabajando = false;
        } else {
          trabajando = false;
        }
      } else {
        _simulation(service);
      }
    }
  }

  Future<double> distance(x1, y1, x2, y2) async {
    return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
  }

  Future<double> shortestDistance(x1, y1, x2, y2, x3, y3) async {
    var px = x2 - x1;
    var py = y2 - y1;
    var temp = (px * px) + (py * py);
    var u = ((x3 - x1) * px + (y3 - y1) * py) / (temp);
    if (u > 1) {
      u = 1;
    } else if (u < 0) {
      u = 0;
    }
    var x = x1 + u * px;
    var y = y1 + u * py;

    var dx = x - x3;
    var dy = y - y3;
    double dist = sqrt(dx * dx + dy * dy);
    return dist;
  }

  Future<GeoPoint> updateLocation(step) async {
    var m = (step.endLocation.lng - step.startLocation.lng) /
        (step.endLocation.lat - step.startLocation.lat);
    var sq = sqrt(1 + m * m);
    var d = 0.0005 / sq;
    var x = step.startLocation.lat + d;
    var y = m * (x - step.startLocation.lat) + step.startLocation.lng;
    var x2 = step.startLocation.lat - d;
    var y2 = m * (x2 - step.startLocation.lat) + step.startLocation.lng;
    var dist1 = await shortestDistance(
        step.startLocation.lat,
        step.startLocation.lng,
        step.endLocation.lat,
        step.endLocation.lng,
        x,
        y);
    var dist2 = await shortestDistance(
        step.startLocation.lat,
        step.startLocation.lng,
        step.endLocation.lat,
        step.endLocation.lng,
        x2,
        y2);
    double xActual = 0;
    double yActual = 0;
    if (dist1 > dist2) {
      xActual = x2;
      yActual = y2;
    } else if (dist2 > dist1) {
      xActual = x;
      yActual = y;
    }
    return GeoPoint(xActual, yActual);
  }
}
