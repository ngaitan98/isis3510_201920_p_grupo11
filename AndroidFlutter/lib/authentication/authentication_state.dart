import 'package:firebase_repository/firebase_repository.dart';
import 'package:meta/meta.dart';
import 'package:user_repository/user_repository.dart';

abstract class AuthenticationState {}

class AuthenticationUninitialized extends AuthenticationState {
  @override
  String toString() => 'AuthenticationUninitialized';
}

class AuthenticationAuthenticated extends AuthenticationState {
  final Service service;
  final UserInfo user;
  AuthenticationAuthenticated({@required this.user, @required this.service});
  @override
  String toString() => 'AuthenticationAuthenticated { User: $user }';
}

class AuthenticationUnauthenticated extends AuthenticationState {
  @override
  String toString() => 'AuthenticationUnauthenticated';
}

class AuthenticationLoading extends AuthenticationState {
  @override
  String toString() => 'AuthenticationLoading';
}
