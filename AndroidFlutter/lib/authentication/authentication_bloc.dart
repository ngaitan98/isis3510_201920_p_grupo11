import 'dart:async';

import 'package:firebase_repository/firebase_repository.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:qpark/authentication/authentication.dart';
import 'package:user_repository/user_repository.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository userRepository;
  final FireBaseRepository firebaseRepository;
  AuthenticationBloc(
      {@required this.userRepository, @required this.firebaseRepository})
      : assert(userRepository != null);

  @override
  AuthenticationState get initialState => AuthenticationUninitialized();

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      final bool hasUser = await userRepository.hasUser();
      if (hasUser) {
        final UserInfo user = await userRepository.getUser();
        if (user.rol != "guest") {
          //Aqui se verifica si tiene un servicio actual.
          Service servicio =
              await firebaseRepository.darServicioActual(user.email);
          yield AuthenticationAuthenticated(user: user, service: servicio);
        } else {
          yield AuthenticationUnauthenticated();
        }
      } else {
        yield AuthenticationUnauthenticated();
      }
    }
    if (event is LoggedIn) {
      yield AuthenticationLoading();
      await userRepository.persistUserInfo(event.user);
      Service servicio =
          await firebaseRepository.darServicioActual(event.user.email);
      yield AuthenticationAuthenticated(user: event.user, service: servicio);
    }

    if (event is LoggedOut) {
      yield AuthenticationLoading();
      await userRepository.logOut();
      yield AuthenticationUnauthenticated();
    }
  }
}
