import 'package:meta/meta.dart';

import 'package:user_repository/user_repository.dart';

abstract class AuthenticationEvent {}

class AppStarted extends AuthenticationEvent {
  @override
  String toString() => 'AppStarted';
}

class LoggedIn extends AuthenticationEvent {
  final UserInfo user;

  LoggedIn({@required this.user});

  @override
  String toString() => 'LoggedIn { User: $user }';
}

class LoggedOut extends AuthenticationEvent {
  @override
  String toString() => 'LoggedOut';
}
