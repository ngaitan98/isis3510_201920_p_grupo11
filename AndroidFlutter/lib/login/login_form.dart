import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qpark/login/login.dart';
import 'package:qpark/navigator/navigator.dart';
import 'package:qpark/common/regular_expresions.dart';

class LoginForm extends StatefulWidget {
  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  ScaffoldFeatureController _snack;

  final _formKey = new GlobalKey<FormState>();
  String _email;
  String _password;
  @override
  Widget build(BuildContext context) {
    final LoginBloc loginBloc = BlocProvider.of<LoginBloc>(context);
    final NavigatorBloc navigatorBloc = BlocProvider.of<NavigatorBloc>(context);
    _onLoginButtonPressed() {
      loginBloc.dispatch(LoginButtonPressed(
        type: "Email",
        email: _email,
        password: _password,
      ));
    }

    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginFailure) {
          try {
            _snack.close();
          } catch (error) {}
          _snack = Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text('${state.error}'),
              backgroundColor: Colors.red,
              duration: Duration(seconds: 2),
            ),
          );
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          return Container(
            padding: EdgeInsets.all(16.0),
            child: new Form(
              key: _formKey,
              child: new ListView(shrinkWrap: true, children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                  child: new TextFormField(
                    maxLines: 1,
                    keyboardType: TextInputType.emailAddress,
                    autofocus: false,
                    maxLength: 50,
                    decoration: new InputDecoration(
                        hintText: 'Correo',
                        icon: new Icon(
                          Icons.mail,
                          color: Colors.grey,
                        )),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'El correo no puede estar vacío';
                      } else if (!alphaNumericHyphen.hasMatch(value)) {
                        return 'El valor ingresado no es válido!';
                      }
                      return null;
                    },
                    onChanged: (value) => _email = value.trim(),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                  child: new TextFormField(
                    maxLines: 1,
                    obscureText: true,
                    autofocus: false,
                    maxLength: 50,
                    decoration: new InputDecoration(
                        hintText: 'Contraseña',
                        icon: new Icon(
                          Icons.lock,
                          color: Colors.grey,
                        )),
                    validator: (value) =>
                        value.isEmpty ? 'Contraseña no puede esta vacía' : null,
                    onChanged: (value) => _password = value.trim(),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                  child: Column(children: [
                    Padding(
                        padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                        child: SizedBox(
                          height: 40.0,
                          child: new RaisedButton(
                            elevation: 5.0,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0)),
                            color: Theme.of(context).primaryColor,
                            child: new Text('Iniciar Sesión',
                                style: new TextStyle(
                                    fontSize: 20.0, color: Colors.white)),
                            onPressed: () {
                              if (_formKey.currentState.validate() &&
                                  state is LoginInitial) {
                                _formKey.currentState.save();
                                _onLoginButtonPressed();
                              }
                            },
                          ),
                        )),
                    Padding(
                        padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                        child: SizedBox(
                          height: 40.0,
                          child: new RaisedButton(
                            elevation: 5.0,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0)),
                            color: Theme.of(context).primaryColor,
                            child: new Text('Registrarse',
                                style: new TextStyle(
                                    fontSize: 20.0, color: Colors.white)),
                            onPressed: () {
                              navigatorBloc.dispatch(NavigateToRegister());
                            },
                          ),
                        ))
                  ]),
                ),
              ]),
            ),
          );
        },
      ),
    );
  }
}
