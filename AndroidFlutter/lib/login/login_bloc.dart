import 'dart:async';

import 'package:connection_status/connection_status.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:qpark/authentication/authentication.dart';
import 'package:qpark/login/login.dart';
import 'package:user_repository/user_repository.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository userRepository;
  final AuthenticationBloc authenticationBloc;
  final ConnectionStatus connectionStatus;
  LoginBloc({
    @required this.connectionStatus,
    @required this.userRepository,
    @required this.authenticationBloc,
  })  : assert(userRepository != null),
        assert(authenticationBloc != null);

  @override
  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      if (connectionStatus == null || !connectionStatus.hasConnection) {
        yield LoginFailure(error: "No hay conexión a internet!");
        await Future.delayed(Duration(seconds: 1));
      } else if (!(event.type == "Email" &&
          event.email == "" &&
          event.password == "")) {
        yield LoginLoading();
        try {
          final UserInfo user = await userRepository.authenticate(
            type: event.type,
            email: event.email,
            password: event.password,
          );
          await Future.delayed(Duration(seconds: 1));
          authenticationBloc.dispatch(LoggedIn(user: user));
        } catch (error) {
          if (error.toString().contains("ERROR_INVALID_EMAIL")) {
            yield LoginFailure(error: "El correo ingresado no es válido");
          } else if (error.toString().contains("ERROR_WRONG_PASSWORD")) {
            yield LoginFailure(error: "Clave Errónea!");
          } else if (error.toString().contains("ERROR_USER_NOT_FOUND")) {
            yield LoginFailure(error: "El correo ingresado no existe!");
          } else if (error.toString().contains("ERROR_USER_DISABLED")) {
            yield LoginFailure(error: "El correo se encuentra deshabilitado!");
          } else if (error.toString().contains("ERROR_TOO_MANY_REQUESTS")) {
            yield LoginFailure(error: "Demasiados intentos! Intente más tarde");
          } else if (error.toString().contains("ERROR_OPERATION_NOT_ALLOWED")) {
            yield LoginFailure(error: "Operación Invalida!");
          }
        }
      }
      yield LoginInitial();
    }
  }
}
