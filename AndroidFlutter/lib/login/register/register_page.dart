import 'package:connection_status/connection_status.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qpark/login/register/register.dart';
import 'package:user_repository/user_repository.dart';

import 'package:qpark/navigator/navigator_bloc.dart';

class RegisterPage extends StatelessWidget {
  RegisterPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final UserRepository userRepository =
        RepositoryProvider.of<UserRepository>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Registrarse'),
      ),
      body: BlocProvider(
        builder: (context) {
          return RegisterBloc(
            connectionStatus: RepositoryProvider.of<ConnectionStatus>(context),
            navigatorBloc: BlocProvider.of<NavigatorBloc>(context),
            userRepository: userRepository,
          );
        },
        child: RegisterForm(),
      ),
    );
  }
}
