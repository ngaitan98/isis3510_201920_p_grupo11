import 'dart:async';

import 'package:connection_status/connection_status.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:qpark/login/register/register.dart';
import 'package:qpark/navigator/navigator_action.dart';

import 'package:user_repository/user_repository.dart';

import 'package:qpark/navigator/navigator.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final UserRepository userRepository;
  final ConnectionStatus connectionStatus;
  final NavigatorBloc navigatorBloc;
  RegisterBloc(
      {@required this.connectionStatus,
      @required this.userRepository,
      @required this.navigatorBloc})
      : assert(userRepository != null),
        assert(navigatorBloc != null);

  @override
  RegisterState get initialState => RegisterInitial();

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is RegisterButtonPressed) {
      if (connectionStatus == null || !connectionStatus.hasConnection) {
        yield RegisterFailure(error: "No hay conexión a internet!");
      } else if (event.password != event.password2) {
        yield RegisterFailure(error: "Las claves no coinciden");
      } else {
        try {
          //Envía petición al back para crear el usuario
          await userRepository.register(
              userInfo: event.user, password: event.password);
          navigatorBloc.dispatch(NavigatorActionPop(null));
        } catch (error) {
          if (error.toString().contains("ERROR_EMAIL_ALREADY_IN_USE")) {
            yield RegisterFailure(
                error: "Ya existe un usuario registrado con este correo!");
          } else if (error.toString().contains("ERROR_WEAK_PASSWORD")) {
            yield RegisterFailure(error: "La clave es muy corta!");
          } else if (error.toString().contains("ERROR_INVALID_EMAIL")) {
            yield RegisterFailure(error: "El correo no es válido!");
          } else {
            yield RegisterFailure(error: error.toString());
          }
        }
      }
      await Future.delayed(Duration(seconds: 1));
      yield RegisterInitial();
    }
  }
}
