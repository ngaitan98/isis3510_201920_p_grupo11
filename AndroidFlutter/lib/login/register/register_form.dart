import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qpark/common/regular_expresions.dart';
import 'package:qpark/login/register/register.dart';
import 'package:user_repository/user_repository.dart';

class RegisterForm extends StatefulWidget {
  @override
  State<RegisterForm> createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  ScaffoldFeatureController _snack;
  final _formKey = new GlobalKey<FormState>();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _lastName = TextEditingController();
  final TextEditingController _name = TextEditingController();
  final TextEditingController _phone = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final TextEditingController _password2 = TextEditingController();
  String _gender = "Masculino";
  int _age;
  @override
  Widget build(BuildContext context) {
    final RegisterBloc registerBloc = BlocProvider.of<RegisterBloc>(context);
    _onRegisterButtonPressed() {
      registerBloc.dispatch(RegisterButtonPressed(
        user: new UserInfo(
            email: _email.text,
            rol: null,
            age: _age,
            gender: _gender,
            lastName: _lastName.text,
            name: _name.text,
            phone: _phone.text),
        password: _password.text,
        password2: _password2.text,
      ));
    }

    return BlocListener<RegisterBloc, RegisterState>(
      listener: (context, state) {
        if (state is RegisterFailure) {
          try {
            _snack.close();
          } catch (error) {}
          _snack = Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text('${state.error}'),
              backgroundColor: Colors.red,
              duration: Duration(seconds: 2),
            ),
          );
        }
      },
      child: BlocBuilder<RegisterBloc, RegisterState>(
        builder: (context, state) {
          return new Container(
              padding: EdgeInsets.all(16.0),
              child: new Form(
                key: _formKey,
                child: new ListView(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: new TextFormField(
                        maxLines: 1,
                        keyboardType: TextInputType.emailAddress,
                        autofocus: false,
                        maxLength: 50,
                        controller: _email,
                        decoration: new InputDecoration(
                            hintText: 'Correo',
                            icon: new Icon(
                              Icons.mail,
                              color: Colors.grey,
                            )),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'El correo no puede estar vacío';
                          } else if (!alphaNumericHyphen.hasMatch(value)) {
                            return 'El valor ingresado no es válido!';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: new TextFormField(
                        maxLines: 1,
                        obscureText: true,
                        autofocus: false,
                        maxLength: 50,
                        controller: _password,
                        decoration: new InputDecoration(
                            hintText: 'Contraseña',
                            icon: new Icon(
                              Icons.lock,
                              color: Colors.grey,
                            )),
                        validator: (value) => value.isEmpty
                            ? 'Contraseña no puede esta vacía'
                            : null,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: new TextFormField(
                        maxLines: 1,
                        obscureText: true,
                        autofocus: false,
                        maxLength: 50,
                        controller: _password2,
                        decoration: new InputDecoration(
                            hintText: 'Contraseña',
                            icon: new Icon(
                              Icons.lock,
                              color: Colors.grey,
                            )),
                        validator: (value) => value.isEmpty
                            ? 'Contraseña no puede esta vacía'
                            : null,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: new TextFormField(
                        maxLines: 1,
                        keyboardType: TextInputType.phone,
                        autofocus: false,
                        maxLength: 10,
                        controller: _phone,
                        decoration: new InputDecoration(
                            hintText: 'Teléfono',
                            icon: new Icon(
                              Icons.phone,
                              color: Colors.grey,
                            )),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'El teléfono no puede estar vacío';
                          } else if (!telephone.hasMatch(value)) {
                            return 'El valor ingresado no es válido!';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: new TextFormField(
                        maxLines: 1,
                        keyboardType: TextInputType.number,
                        autofocus: false,
                        maxLength: 3,
                        decoration: new InputDecoration(
                            hintText: 'Edad',
                            icon: new Icon(
                              Icons.calendar_today,
                              color: Colors.grey,
                            )),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'La edad no puede estar vacía';
                          } else if (!numeric.hasMatch(value)) {
                            return 'El valor ingresado no es válido!';
                          }
                          return null;
                        },
                        onChanged: (value) => _age = int.parse(value),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: new TextFormField(
                        maxLines: 1,
                        keyboardType: TextInputType.text,
                        autofocus: false,
                        maxLength: 50,
                        controller: _name,
                        decoration: new InputDecoration(
                            hintText: 'Nombre',
                            icon: new Icon(
                              Icons.person,
                              color: Colors.grey,
                            )),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'El nombre no puede estar vacío';
                          } else if (!alpha
                              .hasMatch(value.replaceAll(" ", ""))) {
                            return 'El valor ingresado no es válido!';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: new TextFormField(
                        maxLines: 1,
                        keyboardType: TextInputType.text,
                        autofocus: false,
                        maxLength: 50,
                        controller: _lastName,
                        decoration: new InputDecoration(
                            hintText: 'Apellido',
                            icon: new Icon(
                              Icons.person,
                              color: Colors.grey,
                            )),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'El apellido no puede estar vacío';
                          } else if (!alpha
                              .hasMatch(value.replaceAll(" ", ""))) {
                            return 'El valor ingresado no es válido!';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                      child: Column(children: [
                        Padding(
                            padding: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
                            child: SizedBox(
                              height: 40.0,
                              child: new RaisedButton(
                                elevation: 5.0,
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(30.0)),
                                color: Colors.blue,
                                child: new Text('Registrarse',
                                    style: new TextStyle(
                                        fontSize: 20.0, color: Colors.white)),
                                onPressed: () {
                                  if (_formKey.currentState.validate() &&
                                      state is RegisterInitial) {
                                    _formKey.currentState.save();
                                    _onRegisterButtonPressed();
                                  }
                                },
                              ),
                            ))
                      ]),
                    ),
                  ],
                ),
              ));
        },
      ),
    );
  }
}
