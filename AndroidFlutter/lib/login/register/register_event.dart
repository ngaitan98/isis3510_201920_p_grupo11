import 'package:meta/meta.dart';

import 'package:user_repository/user_repository.dart';

abstract class RegisterEvent {}

class RegisterButtonPressed extends RegisterEvent {
  final UserInfo user;
  final String password;
  final String password2;

  RegisterButtonPressed({
    @required this.user,
    @required this.password,
    @required this.password2,
  });

  @override
  String toString() => 'RegisterButtonPressed { user: $user }';
}
