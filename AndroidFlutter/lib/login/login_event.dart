import 'package:meta/meta.dart';

abstract class LoginEvent {
  LoginEvent([List props = const []]) : super();
}

class LoginButtonPressed extends LoginEvent {
  final String type; //Esto para cuando se implemente con google/fb
  final String email;
  final String password;

  LoginButtonPressed({
    @required this.type,
    this.email,
    this.password,
  });
  @override
  String toString() => 'LoginButtonPressed { email: $email }';
}
