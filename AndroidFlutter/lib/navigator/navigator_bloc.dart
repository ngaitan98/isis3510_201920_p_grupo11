import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:qpark/my_parking_lots/my_parking_lots.dart';
import 'package:qpark/my_parking_lots/parking_lot_edit/parking_lot_edit.dart';
import 'package:qpark/parking_lot_detail/parking_lot_detail.dart';
import 'package:qpark/parking_lot_detail/parking_lot_services/parking_lot_services_page.dart';
import './navigator.dart';

class NavigatorBloc extends Bloc<NavigatorAction, dynamic> {
  final GlobalKey<NavigatorState> navigatorKey;
  NavigatorBloc({this.navigatorKey});

  @override
  dynamic get initialState => 0;

  @override
  Stream<dynamic> mapEventToState(
    NavigatorAction action,
  ) async* {
    if (action is NavigatorActionPop) {
      if (action.returnVal != null) {
        navigatorKey.currentState.pop(action.returnVal);
      } else {
        navigatorKey.currentState.pop();
      }
    } else if (action is NavigateToHome) {
      navigatorKey.currentState.pushNamed("/second");
    } else if (action is NavigateToRegister) {
      navigatorKey.currentState.pushNamed("/register");
    } else if (action is NavigateToSearch) {
      navigatorKey.currentState.pushNamed("/search");
    } else if (action is NavigateToMyParkingLots) {
      navigatorKey.currentState.pushNamed("/myparkinglots", arguments: null);
    } else if (action is NavigateToMyParkingHistory) {
      navigatorKey.currentState.pushNamed("/myparkinghistory");
    } else if (action is NavigateToParkingLotDetail) {
      navigatorKey.currentState.pushNamed("/parkinglotdetail",
          arguments: new ParkingLotArgument(parkingLot: action.parkingLot));
    } else if (action is NavigateToEditParkingLot) {
      navigatorKey.currentState.pushNamed("/parkinglotedit",
          arguments: new ParkingLotEditArguments(
              parkingLot: action.parkingLot, bloc: action.bloc));
    } else if (action is NavigateToAddParkingLot) {
      navigatorKey.currentState
          .pushNamed("/parkinglotadd", arguments: action.bloc);
    } else if (action is NavigateToMapSearch) {
      navigatorKey.currentState.pop();
      //Se tiene la busqueda, ir a ruta con esa info.
    } else if (action is NavigateToReserveParkingLot) {
      navigatorKey.currentState
          .pushNamed(("/parkinglotreserve"), arguments: action.bloc);
    } else if (action is NavigateToServiceDetail){
        print('\n\n\n\n Hola probando la accion \n' + action.toString());
      navigatorKey.currentState
      .pushNamed(('/myparkinghistoryservice'), arguments: 
        new ParkingLotServicesArgument(endHour: action.endHour, payment: action.payment, reservation: action.reservation, startHour: action.startHour)
       );
    }
  }
}
