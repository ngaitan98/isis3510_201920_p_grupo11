import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';
import 'package:firebase_repository/firebase_repository.dart';
import 'package:qpark/my_parking_lots/my_parking_lots.dart';

@immutable
abstract class NavigatorAction {}

class NavigatorActionPop extends NavigatorAction {
  final returnVal;
  NavigatorActionPop(this.returnVal);
  @override
  String toString() => 'NavigatorActionPop { ReturnVal: $returnVal }';
}

class NavigateToHome extends NavigatorAction {
  @override
  String toString() => 'NavigateToHome';
}

class NavigateToRegister extends NavigatorAction {
  @override
  String toString() => 'NavigateToRegister';
}

class NavigateToSearch extends NavigatorAction {
  @override
  String toString() => 'NavigateToSearch';
}

class NavigateToMapSearch extends NavigatorAction {
  final String text;
  NavigateToMapSearch(this.text);
  @override
  String toString() => 'NavigateToMapSearch { Text: $text }';
}

class NavigateToMyParkingLots extends NavigatorAction {
  @override
  String toString() => 'NavigateToMyParkingLots';
}

class NavigateToMyParkingHistory extends NavigatorAction {
  @override
  String toString() => 'NavigateToMyParkingHistory';
}

class NavigateToParkingLotDetail extends NavigatorAction {
  final ParkingLot parkingLot;
  NavigateToParkingLotDetail({@required this.parkingLot});
  @override
  String toString() => 'NavigateToParkingLotDetail { ParkingLot: $parkingLot }';
}

class NavigateToEditParkingLot extends NavigatorAction {
  final ParkingLot parkingLot;
  final MyParkingLotsBloc bloc;
  NavigateToEditParkingLot({@required this.parkingLot, @required this.bloc});
  @override
  String toString() => 'NavigateToEditParkingLot { ParkingLot: $parkingLot }';
}

class NavigateToAddParkingLot extends NavigatorAction {
  final bloc;

  NavigateToAddParkingLot(this.bloc);
  @override
  String toString() => 'NavigateToAddParkingLot';
}

class NavigateToReserveParkingLot extends NavigateToParkingLotDetail {
  final bloc;
  final parkingLot;
  NavigateToReserveParkingLot({@required this.bloc, @required this.parkingLot});
  @override
  String toString() => 'NavigateToReserveParkingLot { ParkingLot: $parkingLot}';
}

class NavigateToServiceDetail extends NavigatorAction {
  final startHour;
  final endHour;
  final payment;
  final reservation;
  NavigateToServiceDetail ({ @required this.startHour, @required this.endHour, @required this.payment, @required this.reservation});
  @override
  String toString() {
    // TODO: implement toString
    return 'NavigateToServiceDetail { StartHour: $startHour EndHour: $endHour Payment: $payment Reservation: $reservation}';
  }
}
