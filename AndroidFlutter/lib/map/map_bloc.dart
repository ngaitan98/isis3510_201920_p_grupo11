import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_repository/firebase_repository.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:qpark/home/home.dart';
import './map.dart';

class MapsBloc extends Bloc<MapsEvent, MapsState> {
  final FireBaseRepository firebaseRepository;
  MapsBloc({@required this.firebaseRepository});
  GeoPoint initGeo = GeoPoint(4.6665185, -74.1037039);
  @override
  MapsState get initialState => MapInitial(location: initGeo, zoom: 11.25);

  @override
  Stream<MapsState> mapEventToState(
    MapsEvent event,
  ) async* {
    if (event is MapInitialEvent) {
      event.homeBloc.dispatch(CheckConnection());
    } else if (event is LoadViewEvent) {
      if (event.service == null) {
        yield MapParkingLotsState(location: initGeo, zoom: 11.25);
      } else if (event.service.tipo == "PARKED") {
        yield MapParkedState(
            location: initGeo, zoom: 11.25, service: event.service);
      } else if (event.service.tipo == "VALET") {
        yield MapValetState(
            location: initGeo, zoom: 11.25, service: event.service);
      }
    } else if (event is UpdateValetEvent) {
      yield MapValetState(
          location: event.service.locationCar,
          zoom: 11.25,
          service: event.service);
    } else if (event is MapLoaderEvent) {
      if (event.type == 1) {
        yield MapSearchingState(location: event.location, zoom: 11.25);
      } else if (event.type == 2) {
        yield MapWaitingState(location: event.location, zoom: 11.25);
      }
    }
  }
}
