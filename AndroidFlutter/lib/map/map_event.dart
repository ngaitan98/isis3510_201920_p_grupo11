import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_repository/firebase_repository.dart';
import 'package:meta/meta.dart';
import 'package:qpark/home/home.dart';

@immutable
abstract class MapsEvent {}

class MapInitialEvent extends MapsEvent {
  final HomeBloc homeBloc;
  MapInitialEvent(this.homeBloc);
  @override
  String toString() => 'MapInitialEvent';
}

class LoadViewEvent extends MapsEvent {
  final Service service;
  LoadViewEvent({@required this.service});

  @override
  String toString() => 'LoadViewEvent';
}

class UpdateValetEvent extends MapsEvent {
  final ServiceValet service;
  UpdateValetEvent({@required this.service});
  @override
  String toString() => 'UpdateValetEvent';
}

class MapLoaderEvent extends MapsEvent {
  final GeoPoint location;
  final int type;
  MapLoaderEvent({@required this.location, @required this.type});
  @override
  String toString() => 'MapLoaderEvent';
}
