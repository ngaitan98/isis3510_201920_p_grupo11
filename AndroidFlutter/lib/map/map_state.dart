import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_repository/firebase_repository.dart';
import 'package:meta/meta.dart';

abstract class MapsState {
  final GeoPoint location;
  final double zoom;

  const MapsState({@required this.location, @required this.zoom});
}

class MapInitial extends MapsState {
  MapInitial({@required location, @required zoom})
      : super(location: location, zoom: zoom);

  @override
  String toString() => 'MapInitial';
}

class MapLoaderState extends MapsState {
  MapLoaderState({@required location, @required zoom})
      : super(location: location, zoom: zoom);

  @override
  String toString() => 'MapLoaderState';
}

class MapSearchingState extends MapLoaderState {
  MapSearchingState({@required location, @required zoom})
      : super(location: location, zoom: zoom);

  @override
  String toString() => 'MapSearchingState';
}

class MapWaitingState extends MapLoaderState {
  MapWaitingState({@required location, @required zoom})
      : super(location: location, zoom: zoom);

  @override
  String toString() => 'MapSearchingState';
}

class MapValetState extends MapsState {
  final ServiceValet service;

  MapValetState({@required location, @required zoom, @required this.service})
      : super(location: location, zoom: zoom);

  @override
  String toString() => 'MapValetState';
}

class MapParkedState extends MapsState {
  final ServiceParked service;
  MapParkedState({@required location, @required zoom, @required this.service})
      : super(location: location, zoom: zoom);

  @override
  String toString() => 'MapParkedState';
}

class MapParkingLotsState extends MapsState {
  MapParkingLotsState({@required location, @required zoom})
      : super(location: location, zoom: zoom);

  @override
  String toString() => 'MapParkingLotsState';
}
