import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_repository/firebase_repository.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:qpark/authentication/authentication.dart';
import 'package:qpark/home/home_bloc.dart';
import 'package:qpark/navigator/navigator.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:user_repository/user_repository.dart';
import '../ServiceManager.dart';
import 'map.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class InitialMap extends StatefulWidget {
  InitialMap(
      {Key key,
      @required this.homeBloc,
      @required this.mapsBloc,
      @required this.serviceManager})
      : super(key: key);
  final HomeBloc homeBloc;
  final MapsBloc mapsBloc;
  final ServiceManager serviceManager;
  @override
  _InitialMapState createState() =>
      _InitialMapState(homeBloc, mapsBloc, serviceManager);
}

class _InitialMapState extends State<InitialMap> {
  Map<String, Marker> _markers = {};
  final Map<String, Polyline> _polyline = {};

  GoogleMapController mapController;
  PanelController _pc = new PanelController();
  final double _initFabHeight = 120.0;
  final HomeBloc homeBloc;
  final MapsBloc mapsBloc;
  final ServiceManager serviceManager;
  _InitialMapState(this.homeBloc, this.mapsBloc, this.serviceManager);
  double _fabHeight;
  double _pos = 0.0;
  double _panelHeightOpen = 575.0;
  double _panelHeightClosed = 95.0;
  @override
  void initState() {
    super.initState();
    _fabHeight = _initFabHeight;
    mapsBloc.dispatch(MapInitialEvent(homeBloc));
  }

  @override
  Widget build(BuildContext context) {
    final MapsBloc mapsBloc = BlocProvider.of<MapsBloc>(context);
    final MapCameraBloc mapCameraBloc = BlocProvider.of<MapCameraBloc>(context);
    return Container(
        child: BlocListener<MapsBloc, MapsState>(
      listener: (BuildContext context, MapsState mapState) {
        if (mapState is MapParkingLotsState) {
          _loadParkingLots();
        } else if (mapState is MapParkedState) {
          _pc.hide();
          _loadParked(mapState.service);
        } else if (mapState is MapValetState) {
          _loadValet(mapState.service);
          if (_pos != 0.0) {
            _pc.setPanelPosition(_pos);
          }
        } else if (mapState is MapLoaderState) {
          _markers.clear();
          _pc.hide();
          _polyline.clear();
        }
      },
      child: BlocListener<MapCameraBloc, MapCameraState>(
          listener: (BuildContext context, MapCameraState cameraState) {
        if (cameraState is MapLoading) {
          mapController.animateCamera(CameraUpdate.newCameraPosition(
              CameraPosition(
                  target: LatLng(cameraState.location.latitude,
                      cameraState.location.longitude),
                  zoom: cameraState.zoom)));
        } else if (cameraState is MoveToState) {
          mapController.animateCamera(CameraUpdate.newCameraPosition(
              CameraPosition(
                  target: LatLng(cameraState.location.latitude,
                      cameraState.location.longitude),
                  zoom: cameraState.zoom)));
        }
      }, child: BlocBuilder<MapsBloc, MapsState>(builder: (context, state) {
        var children = <Widget>[];
        children.add(Expanded(
            child: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            SlidingUpPanel(
              maxHeight: _panelHeightOpen,
              minHeight: _panelHeightClosed,
              parallaxEnabled: true,
              parallaxOffset: .5,
              controller: _pc,
              body: _body(state, mapCameraBloc, mapsBloc),
              panel: _panel(state),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(18.0),
                  topRight: Radius.circular(18.0)),
              onPanelSlide: (double pos) => setState(() {
                _pos = pos;
                _fabHeight = pos * (_panelHeightOpen - _panelHeightClosed) +
                    _initFabHeight;
              }),
            ),
            Positioned(
              right: 20.0,
              bottom: state is MapValetState || state is MapParkedState
                  ? _fabHeight
                  : 20,
              child: FloatingActionButton(
                child: Icon(
                  Icons.gps_fixed,
                  color: Theme.of(context).primaryColor,
                ),
                onPressed: () {
                  mapCameraBloc.dispatch(RecenterMapEvent());
                },
                backgroundColor: Colors.white,
              ),
            ),
          ],
        )));
        if (state is MapParkingLotsState) {
          children.add(Padding(
            padding: EdgeInsets.all(8.0),
            child: Column(children: <Widget>[
              Text(
                '¿A donde quieres ir?',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(
                height: 8,
              ),
              hacerInput()
            ]),
          ));
        }
        return Scaffold(
          body: Column(children: children),
        );
      })),
    ));
  }

  Widget _panel(state) {
    if (state is MapValetState) {
      if (state.service.type == "COMMING") {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 12.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 30,
                  height: 5,
                  decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.all(Radius.circular(12.0))),
                ),
              ],
            ),
            SizedBox(
              height: 18.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Valet En Camino",
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 24.0,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  state.service.valet.nombre + " está en camino.",
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Hora de inicio: " + state.service.horaInicio,
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              RaisedButton(
                elevation: 5.0,
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0)),
                color: Theme.of(context).primaryColor,
                child: new Text('Llamar',
                    style: new TextStyle(fontSize: 20.0, color: Colors.white)),
                onPressed: () {
                  RepositoryProvider.of<UserRepository>(context)
                      .call(state.service.valet.numero);
                },
              ),
            ]),
            SizedBox(
              height: 10.0,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top: 8.0, bottom: 20),
                  child: RaisedButton(
                    elevation: 5.0,
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    color: Colors.red,
                    child: new Text('Cancelar',
                        style:
                            new TextStyle(fontSize: 20.0, color: Colors.white)),
                    onPressed: () {
                      serviceManager.cancelar();
                    },
                  ))
            ])
          ],
        );
      } else if (state.service.type == "PARKING") {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 12.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 30,
                  height: 5,
                  decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.all(Radius.circular(12.0))),
                ),
              ],
            ),
            SizedBox(
              height: 18.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Valet En Camino",
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 24.0,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  state.service.valet.nombre +
                      " está en camino al parqueadero.",
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Hora de inicio: " + state.service.horaInicio,
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              RaisedButton(
                elevation: 5.0,
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0)),
                color: Theme.of(context).primaryColor,
                child: new Text('Llamar',
                    style: new TextStyle(fontSize: 20.0, color: Colors.white)),
                onPressed: () {
                  RepositoryProvider.of<UserRepository>(context)
                      .call(state.service.valet.numero);
                },
              ),
            ]),
            SizedBox(
              height: 10.0,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top: 8.0, bottom: 20),
                  child: RaisedButton(
                    elevation: 5.0,
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    color: Colors.red,
                    child: new Text('Cancelar',
                        style:
                            new TextStyle(fontSize: 20.0, color: Colors.white)),
                    onPressed: () {
                      serviceManager.cancelar();
                    },
                  ))
            ])
          ],
        );
      } else if (state.service.type == "RETURNING") {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 12.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 30,
                  height: 5,
                  decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.all(Radius.circular(12.0))),
                ),
              ],
            ),
            SizedBox(
              height: 18.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Valet En Camino",
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 24.0,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  state.service.valet.nombre + "está en camino.",
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Hora de inicio: " + state.service.horaInicio,
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              RaisedButton(
                elevation: 5.0,
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0)),
                color: Theme.of(context).primaryColor,
                child: new Text('Llamar',
                    style: new TextStyle(fontSize: 20.0, color: Colors.white)),
                onPressed: () {
                  RepositoryProvider.of<UserRepository>(context)
                      .call(state.service.valet.nombre);
                },
              ),
            ]),
            SizedBox(
              height: 10.0,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top: 8.0, bottom: 20),
                  child: RaisedButton(
                    elevation: 5.0,
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    color: Colors.red,
                    child: new Text('Cancelar',
                        style:
                            new TextStyle(fontSize: 20.0, color: Colors.white)),
                    onPressed: () {
                      serviceManager.cancelar();
                    },
                  ))
            ])
          ],
        );
      }
    } else {
      return SizedBox.shrink();
    }
  }

  _body(state, mapCameraBloc, mapsBloc) {
    GeoPoint location = state.location;
    return BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (authContext, authState) {
      return GoogleMap(
        compassEnabled: false,
        mapType: MapType.normal,
        myLocationEnabled: true,
        myLocationButtonEnabled: false,
        initialCameraPosition: CameraPosition(
            target: LatLng(location.latitude, location.longitude), zoom: 11.25),
        onMapCreated: (controller) {
          mapController = controller;
          if (authState is AuthenticationAuthenticated) {
            mapsBloc.dispatch(LoadViewEvent(service: authState.service));
            if (authState.service != null &&
                authState.service is ServiceValet) {
              serviceManager.restartSim(authState.service);
            }
            mapCameraBloc.dispatch(MapCameraInitialEvent());
          }
        },
        markers: _markers.values.toSet(),
        polylines: _polyline.values.toSet(),
      );
    });
  }

  _loadValet(ServiceValet service) async {
    _pc.show();
    _markers.clear();
    _polyline.clear();

    if (service.steps.length > 0) {
      List<LatLng> polyTemp = List();
      service.steps.forEach((s) {
        polyTemp.add(LatLng(s.startLocation.lat, s.startLocation.lng));
      });
      polyTemp.add(LatLng(
          service.steps[service.steps.length - 1].endLocation.lat,
          service.steps[service.steps.length - 1].endLocation.lng));
      _polyline["Polyline"] = Polyline(
        width: 5,
        polylineId: PolylineId("Polyline"),
        visible: true,
        points: polyTemp,
        color: Colors.blue,
      );
    }
    if (service.locationCar != null) {
      final marker = Marker(
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
        markerId: MarkerId("LocationCar"),
        position:
            LatLng(service.locationCar.latitude, service.locationCar.longitude),
      );
      _markers["LocationCar"] = marker;
    }
    if (service.locationDestination != null) {
      final marker = Marker(
        markerId: MarkerId("LocationDestination"),
        position: LatLng(service.locationDestination.latitude,
            service.locationDestination.longitude),
      );
      _markers["LocationDestination"] = marker;
    }
  }

  _loadParked(ServiceParked service) async {
    _pc.show();
    _markers.clear();
    _polyline.clear();
    final marker = Marker(
        markerId: MarkerId("PARKED"),
        position:
            LatLng(service.locationCar.latitude, service.locationCar.longitude),
        infoWindow: InfoWindow(
          title: "NOMBRE",
          snippet: 'Costo: 000',
          onTap: () {
            serviceManager.simulateBringBack(
                service.locationCar, service.horaInicio, service.valet);
          },
        ));
    _markers["PARKED"] = marker;
  }

  _loadParkingLots() async {
    _markers.clear();
    _pc.hide();
    _polyline.clear();
    final NavigatorBloc navigatorBloc = BlocProvider.of<NavigatorBloc>(context);
    final FireBaseRepository fireBaseRepository =
        RepositoryProvider.of<FireBaseRepository>(context);
    for (final office in await fireBaseRepository.getParkingLots()) {
      final marker = Marker(
        markerId: MarkerId(office.name),
        position: LatLng(office.location.latitude, office.location.longitude),
        infoWindow: InfoWindow(
            title: office.name,
            snippet: 'Costo: ${office.costPerMinute}',
            onTap: () {
              navigatorBloc.dispatch(
                NavigateToParkingLotDetail(
                  parkingLot: ParkingLot(
                      address: office.address,
                      id: office.id,
                      localidad: office.localidad,
                      location: office.location,
                      name: office.name,
                      costPerMinute: office.costPerMinute,
                      owner: office.owner,
                      parkingSpots: office.parkingSpots,
                      zipCode: office.zipCode),
                ),
              );
            }),
      );
      _markers[office.name] = marker;
    }
    _pc.hide();
  }

  Center hacerInput() {
    return Center(
      child: InkWell(
        child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0), color: Colors.white),
            child: TextField(
              enabled: false,
              decoration: new InputDecoration(
                  hintText: "Buscar...",
                  fillColor: Colors.white,
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(left: 15, top: 15),
                  suffixIcon: IconButton(
                    icon: Icon(Icons.search),
                    iconSize: 30.0,
                    onPressed: () {},
                  )),
            )),
        onTap: () {
          BlocProvider.of<NavigatorBloc>(context).dispatch(NavigateToSearch());
        },
      ),
    );
  }
}
