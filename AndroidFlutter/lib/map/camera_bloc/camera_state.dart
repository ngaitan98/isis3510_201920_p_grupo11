import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';

abstract class MapCameraState {
  final GeoPoint location;
  final double zoom;

  const MapCameraState({@required this.location, @required this.zoom});
}

class MapCameraInitial extends MapCameraState {}

class MapLoading extends MapCameraState {
  MapLoading({@required location, @required permission, @required zoom})
      : super(location: location, zoom: zoom);

  @override
  String toString() => 'MapLoading';
}

class MoveToState extends MapCameraState {
  MoveToState({@required location}) : super(location: location, zoom: 17);
  @override
  String toString() => 'MoveToState';
}
