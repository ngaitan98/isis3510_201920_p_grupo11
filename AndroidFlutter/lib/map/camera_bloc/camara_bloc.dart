import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:location/location.dart';
import 'package:qpark/home/home.dart';
import './camera.dart';

class MapCameraBloc extends Bloc<MapCameraEvent, MapCameraState> {
  Location location = Location();

  @override
  MapCameraState get initialState => MapCameraInitial();

  @override
  Stream<MapCameraState> mapEventToState(
    MapCameraEvent event,
  ) async* {
    if (event is MapCameraInitialEvent) {
      double zoom = 11.25;
      var permission = await location.requestPermission();
      var userLocation;
      if (permission) {
        var temp = await location.getLocation();
        userLocation = GeoPoint(temp.latitude, temp.longitude);
        zoom = 17;
      }
      await Future.delayed(Duration(seconds: 1));
      yield MapLoading(
          location: userLocation, permission: permission, zoom: zoom);
    } else if (event is MoveToEvent) {
      yield MoveToState(location: event.location);
    } else if (event is RecenterMapEvent) {
      try {
        var temp = await location.getLocation();
        yield MoveToState(location: GeoPoint(temp.latitude, temp.longitude));
      } catch (error) {
        var permission = await location.requestPermission();
        if (permission) {
          var temp = await location.getLocation();
          yield MoveToState(location: GeoPoint(temp.latitude, temp.longitude));
        }
      }
    }
  }
}
