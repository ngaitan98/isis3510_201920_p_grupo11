import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';

@immutable
abstract class MapCameraEvent {}

class MapCameraInitialEvent extends MapCameraEvent {}

class MoveToEvent extends MapCameraEvent {
  final GeoPoint location;
  MoveToEvent({@required this.location});
  @override
  String toString() => 'MoveToEvent';
}

class RecenterMapEvent extends MapCameraEvent {}
