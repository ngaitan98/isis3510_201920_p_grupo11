import 'package:flutter/material.dart';

import 'package:bloc/bloc.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qpark/ServiceManager.dart';
import 'package:qpark/authentication/authentication.dart';
import 'package:qpark/common/common.dart';
import 'package:qpark/home/home.dart';
import 'package:qpark/login/login.dart';
import 'package:qpark/map/map.dart';
import 'package:qpark/my_parking_lots/parking_lot_add/parking_lot_add.dart';
import 'package:qpark/navigator/navigator.dart';
import 'package:qpark/parking_lot_detail/parking_lot_detail.dart';
import 'package:qpark/my_parking_lots/parking_lot_edit/parking_lot_edit.dart';
import 'package:qpark/parking_lot_detail/parking_lot_services/bloc.dart';
import 'package:qpark/splash/splash.dart';
import 'package:user_repository/user_repository.dart';
import 'package:firebase_repository/firebase_repository.dart';
import 'package:qpark/login/register/register.dart';
import 'my_parking_history/my_parking_history.dart';
import 'my_parking_lots/my_parking_lots.dart';
import 'package:connection_status/connection_status.dart';

final kGoogleApiKey = "AIzaSyAP3XYup1w_0_euLMx8dbeRPhDz4R-TZiU";

//Intercepta los eventos de Blocs (Para debug,errores,etc.)
class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print('bloc: ${bloc.runtimeType}, event: $event');
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print('bloc: ${bloc.runtimeType}, transition: $transition');
  }

  @override
  void onError(Bloc bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    print('bloc: ${bloc.runtimeType}, error: $error');
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  BlocSupervisor.delegate = SimpleBlocDelegate();
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey();
  final ConnectionStatus connectionStatus = ConnectionStatus.getInstance();
  final FireBaseRepository firebaseRepository = FireBaseRepository();
  final UserRepository userRepository = UserRepository();
  final MapsBloc mapsBloc = MapsBloc(firebaseRepository: firebaseRepository);
  final ServiceManager serviceManager = ServiceManager(
      mapsBloc: mapsBloc,
      fireBaseRepository: firebaseRepository,
      userRepository: userRepository);
  firebaseRepository.init();
  connectionStatus.initialize();

  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  runApp(MultiRepositoryProvider(
      providers: [
        RepositoryProvider<UserRepository>(
          builder: (context) => userRepository,
        ),
        RepositoryProvider<FireBaseRepository>(
          builder: (context) => firebaseRepository,
        ),
        RepositoryProvider<ConnectionStatus>(
          builder: (context) => connectionStatus,
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<AuthenticationBloc>(builder: (context) {
            return AuthenticationBloc(
                firebaseRepository: firebaseRepository,
                userRepository: userRepository)
              ..dispatch(AppStarted());
          }),
          BlocProvider<NavigatorBloc>(builder: (context) {
            return NavigatorBloc(navigatorKey: _navigatorKey);
          }),
          BlocProvider<HomeBloc>(builder: (context) {
            return HomeBloc()..dispatch(InitialEvent(false));
          }),
          BlocProvider<MapCameraBloc>(builder: (context) {
            return MapCameraBloc();
          }),
          BlocProvider<MapsBloc>(builder: (context) {
            return mapsBloc;
          })
        ],
        child: App(
            navigatorKey: _navigatorKey,
            firebaseRepository: firebaseRepository,
            serviceManager: serviceManager),
      )));
}

class App extends StatefulWidget {
  final GlobalKey<NavigatorState> navigatorKey;
  final FireBaseRepository firebaseRepository;
  final ServiceManager serviceManager;
  App(
      {Key key,
      @required this.navigatorKey,
      @required this.firebaseRepository,
      @required this.serviceManager})
      : super(key: key);

  @override
  _AppLifecycleReactorState createState() => _AppLifecycleReactorState(
      navigatorKey: navigatorKey,
      firebaseRepository: firebaseRepository,
      serviceManager: serviceManager);
}

class _AppLifecycleReactorState extends State<App> with WidgetsBindingObserver {
  final GlobalKey<NavigatorState> navigatorKey;
  final ServiceManager serviceManager;
  final FireBaseRepository firebaseRepository;
  _AppLifecycleReactorState(
      {@required this.navigatorKey,
      @required this.firebaseRepository,
      @required this.serviceManager});

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() async {
    await firebaseRepository.save();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigatorKey,
      theme: ThemeData(
          primarySwatch: Colors.blue,
          brightness: Brightness.light,
          accentColor: Colors.blueGrey,
          primaryColor: Color.fromRGBO(31, 31, 54, 1)),
      initialRoute: '/',
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        '/': (context) => BlocBuilder<AuthenticationBloc, AuthenticationState>(
              builder: (context, state) {
                print(state);
                if (state is AuthenticationAuthenticated) {
                  return MaterialApp(
                      title: 'QPark', home: HomePage(serviceManager));
                }
                if (state is AuthenticationUnauthenticated) {
                  return LoginPage();
                }
                if (state is AuthenticationLoading) {
                  return LoadingIndicator();
                }
                return SplashPage(); //Mientras Carga.
              },
            ),
        // When navigating to the "/second" route, build the SecondScreen widget.
        '/register': (context) => RegisterPage(),
        '/search': (context) => SearchList(),
        '/myparkinglots': (context) => MyParkingLots(),
        '/myparkinghistory': (context) => MyParkingHistory(),
        '/myparkinghistoryservice': (context) => ParkingLotServicesDetail(),
        '/parkinglotdetail': (context) => ParkingLotDetail(),
        '/parkinglotedit': (contex) => ParkingLotEditPage(),
        '/parkinglotadd': (context) => ParkingLotAddPage(),
        '/parkinglotreserve': (context) => ParkingLotAddPage(),
      },
    );
  }
}
