RegExp alphaNumeric = RegExp(r'^[a-zA-Z0-9]+$');
RegExp alpha = RegExp(r'^[a-zA-Z\b]+$');
RegExp alphaNumericHyphen = RegExp(r'^[a-zA-Z0-9_\.-@]+$');
RegExp direccion = RegExp(r'^[a-zA-Z0-9-\.#\b]+$');
RegExp telephone = RegExp(r'^[0-9\-]+$');
RegExp numeric = RegExp(r'^[0-9]+$');
