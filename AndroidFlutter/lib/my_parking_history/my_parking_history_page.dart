import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qpark/authentication/authentication.dart';
import 'package:firebase_repository/firebase_repository.dart';
import 'package:qpark/navigator/navigator.dart';
import 'package:qpark/parking_lot_detail/parking_lot_services/bloc.dart';

class MyParkingHistory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final AuthenticationBloc authenticationBloc =
        BlocProvider.of<AuthenticationBloc>(context);
    final AuthenticationAuthenticated authState =
        authenticationBloc.currentState is AuthenticationAuthenticated
            ? authenticationBloc.currentState
            : null;
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Mis Servicios')),
      ),
      body: Container(
          child: StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance
            .collection('services')
            .where("user", isEqualTo: authState.user.email)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Container(
                child: Center(
              child: CircularProgressIndicator(),
            ));
          }
          return ListView.builder(
              itemCount: snapshot.data.documents.length,
              itemBuilder: (context, index) {
                final DocumentSnapshot valu = snapshot.data.documents[index];

                if (valu.data["startHour"] is Timestamp) {
                  final ParkingHistory service =
                      ParkingHistory(fecha: valu.data["startHour"]);
                  String hora = service.fecha.toDate().toString();
                  hora = hora.replaceRange(hora.length - 7, hora.length, "");
                  return Card(
                    child: ListTile(
                      leading: Icon(Icons.local_parking),
                      title: Text(hora),
                      onTap: () {
                        // BlocProvider.of<NavigatorBloc>(context);
                        BlocProvider.of<NavigatorBloc>(context).dispatch(
                            NavigateToServiceDetail(
                                endHour: valu.data["endHour"],
                                payment: valu.data["payment"],
                                reservation: valu.data["reservation"],
                                startHour: valu.data["startHour"]));
                      },
                    ),
                  );
                } else {
                  return Card(
                    child: ListTile(
                      leading: Icon(Icons.local_parking),
                      title: Text("Inválido!" + valu.documentID),
                      onTap: () {},
                      onLongPress: () {},
                    ),
                  );
                }
              });
        },
      )),
    );
  }
}
