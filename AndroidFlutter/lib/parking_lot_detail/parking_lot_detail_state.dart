import 'package:meta/meta.dart';

@immutable
abstract class ParkingLotDetailState {}

class ParkingLotDetailLoadingState extends ParkingLotDetailState {
  @override
  String toString() => 'ParkingLotDetailLoadingState';
}

class ParkingLotDetailLoadedState extends ParkingLotDetailState {
  final parkingLot;

  ParkingLotDetailLoadedState({@required this.parkingLot});

  @override
  String toString() =>
      'ParkingLotDetailLoadedState { ParkingLot: $parkingLot }';
}

class ParkingLotDetailFailureState extends ParkingLotDetailState {
  final String error;
  ParkingLotDetailFailureState({@required this.error});

  @override
  String toString() => 'ParkingLotDetailFailureState { error: $error }';
}

class ParkingLotReserveState extends ParkingLotDetailState {
  final parkingLot;
  final authState;
  ParkingLotReserveState({@required this.parkingLot, @required this.authState});

  @override
  String toString() => 'ParkingLotReserveState { parkingLot: $parkingLot }';
}

class ParkingLotFilaState extends ParkingLotDetailState {
  final parkingLot;
  final authState;
  ParkingLotFilaState({@required this.parkingLot, @required this.authState});

  @override
  String toString() => 'ParkingLotFilaState { parkingLot: $parkingLot }';
}
