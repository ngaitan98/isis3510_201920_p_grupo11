import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_repository/firebase_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qpark/authentication/authentication.dart';
import 'package:qpark/common/screen_reducers.dart';
import 'package:user_repository/user_repository.dart';

import 'parking_lot_detail.dart';

class ParkingLotArgument {
  final ParkingLot parkingLot;
  ParkingLotArgument({@required this.parkingLot});
}

class ParkingLotDetail extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ParkingLotDetailStateful();
  }
}

class ParkingLotDetailStateful extends State<ParkingLotDetail> {
  TimeOfDay horaInicio;
  TimeOfDay horaFin;
  int personasFila = 0;
  String idReserva = null;
  String idFila = null;

  DateTime getHoraInicio() {
    var ahora = DateTime.now();
    return DateTime(
        ahora.year, ahora.month, ahora.day, horaInicio.hour, horaInicio.minute);
  }

  DateTime getHoraFin() {
    var ahora = DateTime.now();
    return DateTime(
        ahora.year, ahora.month, ahora.day, horaFin.hour, horaFin.minute);
  }

  void updateCantFila(int sum) {
    setState(() {
      personasFila += sum;
    });
  }

  void updateHoraFin(TimeOfDay _horaFin) {
    print(_horaFin);
    setState(() {
      horaFin = _horaFin;
    });
  }

  void updateHoraInicio(TimeOfDay _horaInicio) {
    setState(() {
      horaInicio = _horaInicio;
    });
  }

  int getCosto(ParkingLot parkingLot) {
    if (horaInicio != null && horaFin != null) {
      var ahora = DateTime.now();
      var tempInicio = DateTime(ahora.year, ahora.month, ahora.day,
          horaInicio.hour, horaInicio.minute);
      var tempFin = DateTime(
          ahora.year, ahora.month, ahora.day, horaFin.hour, horaFin.minute);
      return tempFin.difference(tempInicio).inMinutes *
          parkingLot.costPerMinute;
    } else {
      return 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    final FireBaseRepository firebaseRepository =
        RepositoryProvider.of<FireBaseRepository>(context);
    final UserRepository userRepository =
        RepositoryProvider.of<UserRepository>(context);
    final AuthenticationBloc authenticationBloc =
        BlocProvider.of<AuthenticationBloc>(context);
    final AuthenticationAuthenticated authState =
        authenticationBloc.currentState is AuthenticationAuthenticated
            ? authenticationBloc.currentState
            : null;
    final ParkingLotDetailBloc parkingLotDetailBloc = ParkingLotDetailBloc(
      firebaseRepository: firebaseRepository,
      userRepository: userRepository,
    );
    final ParkingLotArgument args = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(),
      body: BlocProvider(
        builder: (context) {
          return parkingLotDetailBloc
            ..dispatch(ParkingLotDetailLoadEvent(parkingLot: args.parkingLot));
        },
        child: Container(
          child: BlocListener<ParkingLotDetailBloc, ParkingLotDetailState>(
            listener: (context, state) {
              if (state is ParkingLotDetailFailureState) {
                Scaffold.of(context).showSnackBar(
                  SnackBar(
                    content: Text('${state.error}'),
                    backgroundColor: Colors.red,
                  ),
                );
              }
            },
            child: BlocBuilder<ParkingLotDetailBloc, ParkingLotDetailState>(
                builder: (context, state) {
              if (state is ParkingLotDetailLoadedState) {
                return renderForUser(context, parkingLotDetailBloc,
                    state.parkingLot, userRepository, authState);
              } else if (state is ParkingLotReserveState) {
                return createReserva(context, state.parkingLot,
                    firebaseRepository, parkingLotDetailBloc, authState);
              } else if (state is ParkingLotFilaState) {
                return createFila(context, state.parkingLot, firebaseRepository,
                    parkingLotDetailBloc, authState);
              }
              return Container(
                  child: Center(
                child: CircularProgressIndicator(),
              ));
            }),
          ),
        ),
      ),
    );
  }

  Widget createReserva(
      BuildContext context,
      ParkingLot parkingLot,
      FireBaseRepository fireBaseRepository,
      ParkingLotDetailBloc parkingLotDetailBloc,
      authState) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          height: 20,
        ),
        Text(
          'Reserva',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 20),
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          'Costo por minuto ${parkingLot.costPerMinute}',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 12,
          ),
        ),
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    color: Color.fromRGBO(153, 143, 199, 1),
                    child: SizedBox(
                        width: screenWidth(context, dividedBy: 3),
                        child: Container(
                          alignment: Alignment.center,
                          child: Column(
                            children: <Widget>[
                              FlatButton(
                                child: Text('Hora Inicio'),
                                onPressed: () async {
                                  final TimeOfDay sisa = await showTimePicker(
                                    initialTime: TimeOfDay.now(),
                                    context: context,
                                  );
                                  setState(() {
                                    horaInicio = sisa;
                                  });
                                },
                              ),
                            ],
                          ),
                        )),
                  ),
                  Container(
                    color: Color.fromRGBO(153, 143, 199, 1),
                    child: SizedBox(
                        width: screenWidth(context, dividedBy: 3),
                        child: Container(
                          alignment: Alignment.center,
                          child: Column(
                            children: <Widget>[
                              FlatButton(
                                child: Text('Hora Fin'),
                                onPressed: () async {
                                  final TimeOfDay sisa = await showTimePicker(
                                    initialTime: TimeOfDay.now(),
                                    context: context,
                                  );
                                  setState(() {
                                    horaFin = sisa;
                                  });
                                },
                              ),
                            ],
                          ),
                        )),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              RaisedButton(
                child: Text('Hacer la Reserva'),
                color: Theme.of(context).primaryColor,
                textColor: Colors.white,
                onPressed: () {
                  if (getCosto(parkingLot) > 0) {
                    setState(() {
                      Future temp = fireBaseRepository.createReserva(
                          parkingLot,
                          Reserva(
                              endHour: getHoraFin(),
                              startHour: getHoraInicio(),
                              payment: 'card',
                              status: 'finished'),
                          authState);
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                                title: Text('Servicio Exitoso'),
                                content: Text(
                                    'El costo de la reserva fue: ${getCosto(parkingLot)}'));
                          });
                      temp.then((data) {
                        idReserva = data;
                      });
                    });
                  } else {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                              title: Text('Error en el formato de la hora'),
                              content: Text(
                                  'La hora Inicio: ${this.horaInicio} es anterior a la hora Fin  ${this.horaFin}'));
                        });
                  }
                },
              ),
              Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    ListTile(
                      title: Text(
                        'Reserva Actual',
                      ),
                      leading: Icon(Icons.flag),
                    ),
                    reservaActual(context, fireBaseRepository, parkingLot)
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget reservaActual(BuildContext context,
      FireBaseRepository fireBaseRepository, ParkingLot parkingLot) {
    if (getCosto(parkingLot) > 0) {
      return Column(
        children: <Widget>[
          ListTile(
            title: Text('Hora Inicio: ${this.horaInicio.toString()}'),
          ),
          ListTile(
            title: Text('Hora Inicio: ${this.horaFin.toString()}'),
          ),
          RaisedButton(
            color: Theme.of(context).errorColor,
            child: Text('Cancelar la Reserva'),
            onPressed: () {
              fireBaseRepository.cancelarReserva(idReserva);
              setState(() {
                horaInicio = null;
                horaFin = null;
                idReserva = null;
              });
            },
          )
        ],
      );
    } else {
      return Column(
        children: <Widget>[],
      );
    }
  }

  Widget createFila(
      BuildContext context,
      parkingLot,
      FireBaseRepository firebaseRepository,
      ParkingLotDetailBloc parkingLotDetailBloc,
      AuthenticationAuthenticated authState) {
    firebaseRepository.getPersonasFila(parkingLot).then((onValue) {
      setState(() {
        personasFila = onValue;
      });
    });
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        ListTile(
          title: Text(
            'Fila',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 40),
          ),
        ),
        Text(
          'Tamaño ${this.personasFila}',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 40),
        ),
        RaisedButton(
          child: Text(
            'Entrar a la Fila',
            style: TextStyle(color: Colors.white),
          ),
          color: Theme.of(context).primaryColor,
          onPressed: () {
            if (personasFila == 0) {
              var temp = firebaseRepository.createFila(parkingLot, authState);
              temp.then((onValue) {
                setState(() {
                  idFila = onValue;
                });
              });
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(title: Text('Servicio Exitoso'));
                  });
            } else {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                        title: Text('No se puede entrar dos veces a la fila'));
                  });
            }
          },
        ),
        RaisedButton(
          child:
              Text('Salir de la Fila', style: TextStyle(color: Colors.white)),
          color: Theme.of(context).errorColor,
          onPressed: () => {
            if (personasFila > 0)
              {
                firebaseRepository.salirFila(idFila),
                this.updateCantFila(-1),
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(title: Text('Éxito en la salida'));
                    })
              }
            else
              {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                          title: Text('No hay fila de la cual salir'));
                    })
              }
          },
        )
      ],
    );
  }
}

Column createButtons(
    BuildContext context,
    ParkingLotDetailBloc parkingLotDetailBloc,
    ParkingLot parkingLot,
    authState) {
  return Column(
    mainAxisSize: MainAxisSize.max,
    children: <Widget>[
      createDetail(parkingLot),
      Align(
        alignment: Alignment.center,
        child: ButtonTheme(
          child: Center(
            child: RaisedButton(
              color: Theme.of(context).primaryColor,
              child: Text(
                'Reservar',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => {
                parkingLotDetailBloc.dispatch(ReserveParkingLotEvent(
                    parkingLot: parkingLot,
                    reserva: null,
                    authState: authState))
              },
            ),
          ),
        ),
      ),
      Align(
        alignment: Alignment.bottomCenter,
        child: ButtonTheme(
          child: Center(
            child: RaisedButton(
              color: Theme.of(context).primaryColor,
              child:
                  Text('Revisar Fila', style: TextStyle(color: Colors.white)),
              onPressed: () => {
                parkingLotDetailBloc.dispatch(FilaParkingLotEvent(
                    parkingLot: parkingLot,
                    reserva: null,
                    authState: authState)),
              },
            ),
          ),
        ),
      ),
    ],
  );
}

Column createDetail(ParkingLot parkingLot) {
  return Column(
    children: <Widget>[
      ListView(
        shrinkWrap: true,
        padding: EdgeInsets.all(8.0),
        children: <Widget>[
          ListTile(
            subtitle: Text('Dirección'),
            title: Text('${parkingLot.address}'),
            leading: Icon(
              Icons.local_parking,
              size: 40,
              color: Color.fromRGBO(85, 12, 24, 1),
            ),
          ),
          ListTile(
            leading: Icon(
              Icons.attach_money,
              size: 40,
              color: Color.fromRGBO(85, 12, 24, 1),
            ),
            subtitle: Text('Costo por Minuto'),
            title: Text('${parkingLot.costPerMinute}'),
          ),
          ListTile(
            subtitle: Text('Localidad'),
            title: Text('${parkingLot.localidad}'),
            leading: Icon(
              Icons.room,
              size: 40,
              color: Color.fromRGBO(85, 12, 24, 1),
            ),
          ),
          ListTile(
            subtitle: Text('Puestos Libres'),
            title: Text('${parkingLot.parkingSpots}'),
            leading: Icon(
              Icons.directions_car,
              size: 40,
              color: Color.fromRGBO(85, 12, 24, 1),
            ),
          ),
          ListTile(
            subtitle: Text('Correo del Dueño'),
            title: Text('${parkingLot.owner}'),
            leading: Icon(
              Icons.contact_mail,
              size: 40,
              color: Color.fromRGBO(85, 12, 24, 1),
            ),
          ),
          ListTile(
            subtitle: Text('Código ZIP'),
            title: Text('${parkingLot.zipCode}'),
            leading: Icon(
              Icons.mail,
              size: 40,
              color: Color.fromRGBO(85, 12, 24, 1),
            ),
          ),
        ],
      ),
    ],
  );
}

Widget renderForUser(BuildContext context, ParkingLotDetailBloc bloc,
    ParkingLot parkingLot, UserRepository userRepository, authState) {
  if (authState.user.rol == 'owner') {
    return createDetail(parkingLot);
  }
  return createButtons(context, bloc, parkingLot, authState);
}
