import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_repository/firebase_repository.dart';

abstract class ParkingLotServicesState {
  ParkingLotServicesState([List props = const []]) : super();
}

class InitialParkingLotServicesState extends ParkingLotServicesState {
  final Timestamp startHour;
  final Timestamp endHour;
  final String payment;
  final bool reservation;

  InitialParkingLotServicesState({this.startHour, this.endHour, this.payment, this.reservation });

  @override
  String toString() => 'InitialParkingLotServicesState';
}

class ParkingLotServicesLoadingState extends ParkingLotServicesState {
  final Timestamp startHour;
  final Timestamp endHour;
  final String payment;
  final bool reservation;

  ParkingLotServicesLoadingState({this.startHour, this.endHour, this.payment, this.reservation });

  @override
  String toString() => 'ParkingLotServicesLoadingState';
}

class ParkingLotServicesReloadState extends ParkingLotServicesState {
  final Timestamp startHour;
  final Timestamp endHour;
  final String payment;
  final bool reservation;

  ParkingLotServicesReloadState({this.startHour, this.endHour, this.payment, this.reservation });

  @override
  String toString() => 'ParkingLotServicesReloadState';
}

class ParkingLotServicesLoadedState extends ParkingLotServicesState {
  final Timestamp startHour;
  final Timestamp endHour;
  final String payment;
  final bool reservation;

  ParkingLotServicesLoadedState({this.startHour, this.endHour, this.payment, this.reservation });
  @override
  String toString() => 'ParkingLotServicesReloadState';
}

class ParkingLotServicesFailureState extends ParkingLotServicesState {
  final Timestamp startHour;
  final Timestamp endHour;
  final String payment;
  final bool reservation;

  ParkingLotServicesFailureState({this.startHour, this.endHour, this.payment, this.reservation });

  String error;
  @override
  String toString() => 'ParkingLotServicesFailerState';
}