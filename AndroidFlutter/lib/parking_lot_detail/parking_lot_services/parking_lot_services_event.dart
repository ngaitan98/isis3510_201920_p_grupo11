import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_repository/firebase_repository.dart';
import 'package:flutter/material.dart';
import 'bloc.dart';

abstract class ParkingLotServicesEvent {}

class ParkingLotServicesLoadEvent extends ParkingLotServicesEvent{
  final Timestamp startHour;
  final Timestamp endHour;
  final String payment;
  final bool reservation;

  ParkingLotServicesLoadEvent({@required this.startHour,@required this.endHour,@required this.payment,@required this.reservation});

  @override
  String toString() => 'ParkingLotServiceLoadState { $startHour, $endHour, $payment, $reservation }';
}

class ParkingLotServicesReloadEvent extends ParkingLotServicesEvent {
  final Timestamp startHour;
  final Timestamp endHour;
  final String payment;
  final bool reservation;

  ParkingLotServicesReloadEvent({@required this.startHour,@required this.endHour,@required this.payment,@required this.reservation });

  @override
  String toString() =>
      'ParkingLotDetailReloadState { $startHour, $endHour, $payment, $reservation}';
}

class ParkingLotServicesFailureEvent extends ParkingLotServicesEvent {
  final String error;

  ParkingLotServicesFailureEvent({ this.error });


  @override
  String toString() => 'ParkingLotServicesFailureState { error: $error }';
}
