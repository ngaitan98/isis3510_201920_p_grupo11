import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_repository/firebase_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qpark/authentication/authentication.dart';
import 'package:qpark/common/screen_reducers.dart';
import 'package:user_repository/user_repository.dart';

import 'bloc.dart';

class ParkingLotServicesArgument {
  final startHour;
  final endHour;
  final payment;
  final reservation;
  ParkingLotServicesArgument(
      {@required this.startHour,
      @required this.endHour,
      @required this.payment,
      @required this.reservation});
}

class ParkingLotServicesDetail extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ParkingLotServicesDetailStateFul();
  }
}

class ParkingLotServicesDetailStateFul extends State<ParkingLotServicesDetail> {
  @override
  Widget build(BuildContext context) {
    final FireBaseRepository firebaseRepository =
        RepositoryProvider.of<FireBaseRepository>(context);
    final UserRepository userRepository =
        RepositoryProvider.of<UserRepository>(context);
    final AuthenticationBloc authenticationBloc =
        BlocProvider.of<AuthenticationBloc>(context);
    final AuthenticationAuthenticated authState =
        authenticationBloc.currentState is AuthenticationAuthenticated
            ? authenticationBloc.currentState
            : null;
    final ParkingLotServicesBloc parkingLotDetailBloc = ParkingLotServicesBloc(
      firebaseRepository: firebaseRepository,
      userRepository: userRepository,
    );
    final ParkingLotServicesArgument args = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(),
      body: BlocProvider(
        builder: (context) {
          return parkingLotDetailBloc
            ..dispatch(
                ParkingLotServicesLoadEvent( endHour: args.endHour, payment: args.payment, reservation: args.reservation, startHour: args.startHour));
        },
        child: Container(
          child: BlocListener<ParkingLotServicesBloc, ParkingLotServicesState>(
            listener: (context, state) {
              if (state is ParkingLotServicesFailureEvent) {
                Scaffold.of(context).showSnackBar(
                  SnackBar(
                    content: Text(
                        'No se pudo obtener información sobre el estado del parqueadero, revisa tu conexión a internet.'),
                    backgroundColor: Colors.red,
                  ),
                );
              }
            },
            child: BlocBuilder<ParkingLotServicesBloc, ParkingLotServicesState>(
                builder: (context, state) {
              if (state is ParkingLotServicesLoadedState) {
                return renderForUser(
                    context,
                    parkingLotDetailBloc,
                    state.startHour,
                    state.endHour,
                    state.payment,
                    state.reservation,
                    userRepository,
                    authState);
              }
              return Container(
                  child: Center(
                child: CircularProgressIndicator(),
              ));
            }),
          ),
        ),
      ),
    );
  }
}

Widget renderForUser(
    BuildContext context,
    ParkingLotServicesBloc parkingLotDetailBloc,
    Timestamp startHour,
    Timestamp endHour,
    String payment,
    bool reservation,
    UserRepository userRepository,
    AuthenticationAuthenticated authState) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      SizedBox(
        child: Icon( Icons.local_parking, size: 200),
        ),
      ListTile(
        title: Text('Hora de Inicio'),
        subtitle: startHour != null? Text(startHour.toDate().toIso8601String()): Text('No tiene una hora asignada'),
        leading: Icon(Icons.av_timer),
      ),
      ListTile(
        title: Text('Hora de Finalización'),
        subtitle: endHour != null? Text(endHour.toDate().toIso8601String()): Text('No ha finalizado'),
        leading: Icon(Icons.alarm_off),
      ),
      ListTile(
        title: Text('Forma de Pago'),
        subtitle: payment != null? Text(payment): Text('No ha pagado'),
        leading: Icon(Icons.payment),
      ),
      ListTile(
        title: Text('¿Fue una reserva?'),
        subtitle: reservation != null? Text(reservation.toString()) : Text('No fue una reserva'),
        leading: Icon(Icons.book),
      )
    ],
  );
}
