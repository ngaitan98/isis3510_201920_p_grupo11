import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:firebase_repository/firebase_repository.dart';
import 'package:flutter/material.dart';
import 'package:qpark/navigator/navigator_bloc.dart';
import 'package:user_repository/user_repository.dart';
import './bloc.dart';

class ParkingLotServicesBloc extends Bloc<ParkingLotServicesEvent, ParkingLotServicesState> {
  
  ParkingLotServicesBloc(
      {
        @required this.firebaseRepository,
        @required this.userRepository,
        this.navigatorBloc
        })
      : assert(firebaseRepository != null);

  final FireBaseRepository firebaseRepository;
  final UserRepository userRepository;
  final NavigatorBloc navigatorBloc;
  
  @override
  ParkingLotServicesState get initialState => InitialParkingLotServicesState();

  @override
  Stream<ParkingLotServicesState> mapEventToState(
    ParkingLotServicesEvent event,
  ) async* {
    // TODO: Add Logic
    if ( event is ParkingLotServicesLoadEvent){
      print('\n\n'+event.toString()+'\n\n');
      yield ParkingLotServicesLoadedState(endHour: event.endHour, payment: event.payment, reservation: event.reservation, startHour: event.startHour);
    }
    else if (event is ParkingLotServicesReloadEvent) {
      yield ParkingLotServicesReloadState();
    }
  }
}
