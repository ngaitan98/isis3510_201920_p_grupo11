import 'dart:async';

import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:qpark/navigator/navigator.dart';
import 'parking_lot_detail.dart';
import 'package:firebase_repository/firebase_repository.dart';
import 'package:user_repository/user_repository.dart';

class ParkingLotDetailBloc
    extends Bloc<ParkingLotDetailEvent, ParkingLotDetailState> {
  final FireBaseRepository firebaseRepository;
  final UserRepository userRepository;
  final NavigatorBloc navigatorBloc;

  ParkingLotDetailBloc(
      {@required this.firebaseRepository,
      @required this.userRepository,
      this.navigatorBloc})
      : assert(firebaseRepository != null);

  @override
  ParkingLotDetailState get initialState => ParkingLotDetailLoadingState();

  @override
  Stream<ParkingLotDetailState> mapEventToState(
      ParkingLotDetailEvent event) async* {
    if (event is ParkingLotDetailLoadEvent) {
      yield ParkingLotDetailLoadingState();
      try {
        yield ParkingLotDetailLoadedState(parkingLot: event.parkingLot);
      } catch (error) {
        yield ParkingLotDetailFailureState(error: error.toString());
      }
    } else if (event is ParkingLotDetailReloadEvent) {
      yield ParkingLotDetailLoadingState();
      try {
        final parkingLot =
            firebaseRepository.getParkingLot(event.parkingLot.id);

        yield ParkingLotDetailLoadedState(parkingLot: parkingLot);
      } catch (error) {
        yield ParkingLotDetailFailureState(error: error.toString());
      }
    } else if (event is ReserveParkingLotEvent) {
      yield ParkingLotReserveState(parkingLot: event.parkingLot, authState: event.authState);
    } else if (event is FilaParkingLotEvent) {
      yield ParkingLotFilaState(parkingLot: event.parkingLot, authState: event.authState);
    }
  }
}
