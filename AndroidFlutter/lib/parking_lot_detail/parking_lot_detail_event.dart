import 'package:firebase_repository/firebase_repository.dart';
import 'package:qpark/parking_lot_detail/parking_lot_detail_bloc.dart';

abstract class ParkingLotDetailEvent {}

class ParkingLotDetailLoadEvent extends ParkingLotDetailEvent {
  final ParkingLot parkingLot;
  ParkingLotDetailLoadEvent({this.parkingLot});
  @override
  String toString() => 'ParkingLotDetailLoadEvent { ParkingLot: $parkingLot }';
}

class ParkingLotDetailReloadEvent extends ParkingLotDetailEvent {
  final ParkingLot parkingLot;
  ParkingLotDetailReloadEvent({this.parkingLot});
  @override
  String toString() =>
      'ParkingLotDetailReloadEvent { ParkingLot: $parkingLot }';
}

class ReserveParkingLotEvent extends ParkingLotDetailEvent {
  final ParkingLot parkingLot;
  final Reserva reserva;
  final dynamic authState;
  ReserveParkingLotEvent({ this.parkingLot, this.reserva, this.authState});
  @override
  String toString() {
    // TODO: implement toString
    return 'ReserveParkingLotEvent { ParkingLot: $parkingLot }';
  }

}

class FilaParkingLotEvent extends ParkingLotDetailEvent {
  final ParkingLot parkingLot;
  final Reserva reserva;
  final dynamic authState;
  FilaParkingLotEvent({ this.parkingLot, this.reserva, this.authState});
  @override
  String toString() {
    // TODO: implement toString
    return 'FilaParkingLotEvent { ParkingLot: $parkingLot }';
  }
}
