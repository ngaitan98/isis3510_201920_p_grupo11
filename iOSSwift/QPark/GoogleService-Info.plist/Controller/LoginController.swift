//
//  LoginController.swift
//  QPark
//
//  Created by Nico Gaitan on 10/10/19.
//  Copyright © 2019 Grupo11. All rights reserved.
//

import UIKit
import Firebase
import Network

class LoginController:UIViewController{
    let db = Firestore.firestore()
    let localDb = UserDefaults.init()
    let monitor = NWPathMonitor()
    
    @IBOutlet weak var loginInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    
    static var conn:Bool?
    var label:UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "conn"), object: nil)
                self.UI {
                    if LoginController.conn != nil, !LoginController.self.conn!{
                        if self.label != nil {
                            self.hideToast(toast: self.label!)
                        }
                        self.label = self.showToast(message: "Hay conexión nuevamente", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.green.withAlphaComponent(0.6), noConn: false)
                        LoginController.conn = true
                    }
                    LoginController.conn = true
                }
            } else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "noConn"), object: nil)
                self.UI {
                    LoginController.conn = false
                    
                    self.label = self.showToast(message: "No hay conexión", font: UIFont(name: "HelveticaNeue", size: 10)!, backgroundColor: UIColor.red.withAlphaComponent(0.6), noConn: true)
                }
            }
        }
        if localDb.bool(forKey: "userLogged"){
            self.goToRoot()
        }
        
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        
    }
    
    @IBAction func loginWithEmail(_ sender: UIButton) {
        let email = loginInput.text
        let password = passwordInput.text
        if email != nil && email != "" && password != nil && password != ""{
            login(withEmail:email ?? "",password:password ?? "")
        }
        else{
            print("TODO Handle")
        }
    }
    func login(withEmail email: String, password: String, _ callback: ((Error?) -> ())? = nil){
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if let e = error{
                callback?(e)
                print("TODO Handle")
                return
            }
            self.db.collection("users").document(email).getDocument(){
                (document, error) in
                if let doc = document{
                    self.localDb.set(true, forKey: "userLogged")
                    self.localDb.set(doc.data(), forKey: "user")
                    self.goToRoot()
                    callback?(nil)
                }
                else{
                    print(error ?? "Error")
                }
            }
            
        }
    }
    func goToRoot(){
        let controller:RootController? = self.navigationController?.storyboard?.instantiateViewController(identifier: "rootController")
        navigationController?.pushViewController(controller ?? RootController(), animated: true)
    }
    @IBAction func goToRegister(_sender: UIButton){
        let controller:RegisterController? = self.navigationController?.storyboard?.instantiateViewController(identifier: "registerController")
        navigationController?.pushViewController(controller ?? RegisterController(), animated: true)
    }
}
extension UIViewController {
    
    func showToast(message : String, font: UIFont, backgroundColor: UIColor, noConn: Bool) -> UILabel {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.origin.x + 20, y: self.view.frame.height - 30, width: self.view.frame.width - 40, height: 20))
        toastLabel.backgroundColor = backgroundColor
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 0;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        
        if !noConn {
            
            UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
                toastLabel.alpha = 0.0
            }, completion: {(isCompleted) in
                toastLabel.removeFromSuperview()
            })
        }
        return toastLabel
    }
    
    func hideToast(toast: UILabel) {
        toast.removeFromSuperview()
    }
    
    func BG(_ block: @escaping ()->Void) {
        DispatchQueue.global(qos: .default).async(execute: block)
    }
    
    func UI(_ block: @escaping ()->Void) {
        DispatchQueue.main.async(execute: block)
    }

}
