//
//  MapController.swift
//  QPark
//
//  Created by Nico Gaitan on 10/9/19.
//  Copyright © 2019 Grupo11. All rights reserved.
//

import UIKit
import Firebase

class MenuController: UITableViewController {
    let localDb = UserDefaults()
    let db = Firestore.firestore()
    var data:[MenuOption]!
    @IBOutlet weak var personName: UILabel!
    
    class MenuOption{
        var option: String!
        var image: UIImage!
        var relatedView: String!
        
        init(opt: String, imgPath: String, viewId: String) {
            option = opt
            image = UIImage(named: imgPath)
            relatedView = viewId
        }
    }
    override func viewDidLoad() {
        let role = (localDb.dictionary(forKey: "user") as AnyObject? ?? nil!).object(forKey: "role") as? String ?? "user"
        let name = (localDb.dictionary(forKey: "user") as AnyObject? ?? nil!).object(forKey: "name") as? String ?? "John"
        let lastName = (localDb.dictionary(forKey: "user") as AnyObject? ?? nil!).object(forKey: "lastName") as? String ?? "Doe"
        personName?.text = "\(name) \(lastName)"
        if role == "client"{
            data = [
                MenuOption(opt: "Mi Perfil", imgPath: "avatar", viewId: "profileController"),
                MenuOption(opt: "Mis Servicios", imgPath: "history-clock-button", viewId: "serviceController"),
                MenuOption(opt: "Ayuda", imgPath: "information", viewId: "helpController"),
                MenuOption(opt: "Cerrar Sesión", imgPath: "logout", viewId: "logout")
            ]
        }
        else if role == "owner"{
            data = [
                MenuOption(opt: "Mi Perfil", imgPath: "avatar", viewId: "profileController"),
                MenuOption(opt: "Mis Servicios", imgPath: "history-clock-button", viewId: "serviceController"),
                MenuOption(opt: "Mis Parqueaderos", imgPath: "parking", viewId: "parkingController"),
                MenuOption(opt: "Ayuda", imgPath: "information", viewId: "helpController"),
                MenuOption(opt: "Cerrar Sesión", imgPath: "logout", viewId: "logout")
            ]
        }
        else{
            data = [
                MenuOption(opt: "Mi Perfil", imgPath: "avatar", viewId: "profileController"),
                MenuOption(opt: "Mis Servicios", imgPath: "history-clock-button", viewId: "serviceController"),
                MenuOption(opt: "Mis Parqueaderos", imgPath: "parking", viewId: "parkingController"),
                MenuOption(opt: "Ayuda", imgPath: "information", viewId: "helpController"),
                MenuOption(opt: "Cerrar Sesión", imgPath: "logout", viewId: "logout")
            ]
        }
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        if data != nil{
            return 1
        }
        else{
            return 0
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! MenuOptionController
        cell.option!.text = data[indexPath.row].option
        if data[indexPath.row].option == "logout"{
            cell.option.textColor = .red
        }
        cell.optionImage!.image = data[indexPath.row].image
        cell.opensView = data[indexPath.row].relatedView
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let relatedView:String = data[indexPath.row].relatedView
        if relatedView == "logout"{
            do {
                try Auth.auth().signOut()
                self.localDb.removeObject(forKey: "user")
                self.localDb.removeObject(forKey: "userLogged")
                self.navigationController?.popViewController(animated: false)
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
            }
            return
        }
        else if relatedView == "profileController"{
            let controller:ProfileController? = self.navigationController?.storyboard?.instantiateViewController(identifier: (tableView.cellForRow(at: indexPath) as! MenuOptionController).opensView)
            navigationController?.pushViewController(controller ?? ProfileController(), animated: true)
        }
        else if relatedView == "parkingController"{
            let controller:ParkingController? = self.navigationController?.storyboard?.instantiateViewController(identifier: (tableView.cellForRow(at: indexPath) as! MenuOptionController).opensView)
            navigationController?.pushViewController(controller ?? ParkingController(), animated: true)
        }
        else if relatedView == "serviceController"{
            db.collection("services").whereField("user", isEqualTo: (localDb.dictionary(forKey: "user") as AnyObject? ?? nil!).object(forKey: "email") as? String ?? "none").getDocuments(completion:{ (querySnapshot, err) in
                if let error = err {
                    print("Error getting documents: \(error)")
                    return
                } else {
                    let controller:ServiceController = self.navigationController?.storyboard?.instantiateViewController(identifier: "serviceController") as! ServiceController
                    controller.data = querySnapshot!.documents
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            })
        }
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.count ?? 0
    }
    @IBAction func popMenu(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
