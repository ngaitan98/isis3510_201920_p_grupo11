//
//  RootController.swift
//  QPark
//
//  Created by Nico Gaitan on 10/8/19.
//  Copyright © 2019 Grupo11. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import Firebase
import Reachability
class RootController: UIViewController, CLLocationManagerDelegate,UITextFieldDelegate{
    
    let db = Firestore.firestore()
    
    let locationManager = CLLocationManager()
    
    let localDb = UserDefaults()
    
    var places: GMSPlacesClient = GMSPlacesClient.shared()
    
    let token = GMSAutocompleteSessionToken.init()
    
    let filter = GMSAutocompleteFilter();
    
    @IBOutlet weak var menuButton: UIButton!
    
    @IBOutlet weak var GMAPS: GMSMapView!
    
    @IBOutlet weak var locationField: UITextField!
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var message: UILabel!
    
    var currentLocation = CLLocationCoordinate2D(latitude: 4.624335, longitude: -74.063644)
    
    var user:AnyObject!
    
    var tableView: SearchResultsController!
    
    override func viewDidLoad() {
        containerView.frame.size.height = 1
        super.viewDidLoad()
        filter.type = .noFilter
        self.view.addSubview(self.menuButton)
        places = GMSPlacesClient.shared()
        user = localDb.dictionary(forKey: "user") as AnyObject? ?? nil!
        message.text = "Hola \((user as AnyObject).object(forKey: "name") ?? "Username")"
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        locationManager.distanceFilter = 50
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
            currentLocation = locationManager.location?.coordinate ?? currentLocation
        }
        let camera = GMSCameraPosition.camera(withTarget: currentLocation, zoom: 16.0)
        GMAPS.camera = camera
        GMAPS.settings.myLocationButton = true
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showToastNoConn), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showToastConn), name: NSNotification.Name(rawValue: "conn"), object: nil)
        showMarkers(location: currentLocation)
    }
    
    @objc func showToastNoConn() {
        _=self.showToast(message: "No hay conexión. Las funcionalidades son límitadas.", font: UIFont(name: "AvenirNext", size: 20)!, backgroundColor: .systemRed, noConn: true)
       }
       
       @objc func showToastConn() {
        _=self.showToast(message: "No hay conexión", font: UIFont(name: "AvenirNext", size: 20)!, backgroundColor: .systemRed, noConn: false)
       }

    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        locationField.resignFirstResponder()
        return true;
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //esta funcion cierra el texto cuando se tocaa otro punto
        self.view.endEditing(true)
        
    }
    
    func showMarkers(location:CLLocationCoordinate2D){
        GMAPS.clear()
        db.collection("parkings").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    let temp = (document.get("location")) as! GeoPoint
                    let parkingLocation = CLLocationCoordinate2D(latitude: temp.latitude, longitude: temp.longitude)
                    if self.calculateDistance(from: location, to: parkingLocation) <= 1200.0{
                        let marker = GMSMarker(position: parkingLocation)
                        marker.title = document.get("name") as? String
                        marker.snippet = "Dirección: \(document.get("address") as? String ?? "")\nCosto por minuto: $\(document.get("costPerMinute") ?? 0)"
                        
                        marker.icon = self.imageWithImage(image: UIImage(named: "customMarker")!, scaledToSize: CGSize(width: 33.333333, height: 53.3333333))
                        marker.appearAnimation = .pop
                        marker.map = self.GMAPS
                    }
                }
            }
        }
    }
    func calculateDistance(from location1: CLLocationCoordinate2D, to location2: CLLocationCoordinate2D)-> CLLocationDistance{
        let center = CLLocation(latitude: location1.latitude, longitude: location1.longitude)
        let lot = CLLocation(latitude: location2.latitude, longitude: location2.longitude)
        return center.distance(from: lot)
    }
    func center(location: CLLocationCoordinate2D){
        containerView.frame.size.height = 1
        self.view.frame.origin.y = 0
        locationField.resignFirstResponder()
        GMAPS.animate(toLocation: location)
        currentLocation = location
        showMarkers(location: location)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.frame.origin.y = -self.GMAPS.frame.height
        containerView.frame.size.height = 1000
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
    }
    
    @IBAction func searchPlaces(_ sender: UITextField) {
        places.findAutocompletePredictions(fromQuery: locationField.text ?? "", bounds: nil,boundsMode: GMSAutocompleteBoundsMode.bias, filter: filter, sessionToken: token, callback: { (results, error) in
            if let error = error {
                print("Autocomplete error: \(error)")
                return
            }
            if let results = results {
                self.tableView?.data = results
                self.tableView?.father = self
                self.tableView?.reload()
            }
        })
    }
    @IBAction func pushMenuView(_ sender: UIButton) {
        let controller:MenuController? = self.navigationController?.storyboard?.instantiateViewController(identifier: "menuController")
        navigationController?.pushViewController(controller ?? MenuController(), animated: true)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "searchResultSegue"{
            self.tableView = segue.destination as? SearchResultsController
        }
    }
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}

