//
//  RegisterController.swift
//  QPark
//
//  Created by Nico Gaitan on 10/13/19.
//  Copyright © 2019 Grupo11. All rights reserved.
//

import UIKit
import Firebase

class RegisterController:UIViewController{
    
    let db = Firestore.firestore()
    
    @IBOutlet weak var nameInput: UITextField!
    @IBOutlet weak var lastInput: UITextField!
    @IBOutlet weak var phoneInput: UITextField!
    @IBOutlet weak var emailInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    @IBOutlet weak var confirmInput: UITextField!
    
    @IBOutlet weak var createButton: UIButton!

   
    
    @IBAction func lasEditingChanged(_ sender: Any) {
       
         guard lastInput.text != "",nameInput.text != "",phoneInput.text != "",emailInput.text != "",passwordInput.text != "",confirmInput.text != ""
           else {
               createButton.isUserInteractionEnabled = false
               return
           }
           createButton.isUserInteractionEnabled = true
           self.createButton.backgroundColor =  UIColor.systemGreen
    }
    
    @IBAction func emailEditingChanged(_ sender: Any) {
   
        guard lastInput.text != "",nameInput.text != "",phoneInput.text != "",emailInput.text != "",passwordInput.text != "",confirmInput.text != ""
                 else {
                     createButton.isUserInteractionEnabled = false
                     return
                 }
                 createButton.isUserInteractionEnabled = true
                 self.createButton.backgroundColor =  UIColor.systemGreen
    }
    
    @IBAction func passwordEditingChanged(_ sender: Any) {
        
           guard lastInput.text != "",nameInput.text != "",phoneInput.text != "",emailInput.text != "",passwordInput.text != "",confirmInput.text != ""
                 else {
                     createButton.isUserInteractionEnabled = false
                     return
                 }
                 createButton.isUserInteractionEnabled = true
                 self.createButton.backgroundColor = UIColor.systemGreen
    }
    
    @IBAction func confirmEditingChanged(_ sender: Any) {
    
     guard lastInput.text != "",nameInput.text != "",phoneInput.text != "",emailInput.text != "",passwordInput.text != "",confirmInput.text != ""
             else {
                 createButton.isUserInteractionEnabled = false
                 return
             }
             createButton.isUserInteractionEnabled = true
             self.createButton.backgroundColor = UIColor.systemGreen
    }
    
    
    
    override func viewDidLoad() {
       

        
        super.viewDidLoad()
        createButton.isUserInteractionEnabled = false
         self.createButton.backgroundColor = UIColor.systemGray

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            self.view.frame.origin.y = -keyboardSize.height
        }
   
       
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {

        
        let empty = textField.text?.isEmpty ?? true

        if empty{
            confirmInput.isUserInteractionEnabled = true
        } else {
            confirmInput.isUserInteractionEnabled = false
        }
        return true
    }
    
    
    @IBAction func createAccount(_ sender: UIButton) {

        Auth.auth().createUser(withEmail: self.emailInput.text ?? "", password: self.passwordInput.text ?? ""){(authResult, error) in
            if let user = authResult?.user {
                print(user)
                self.db.collection("users").document(self.emailInput.text ?? "").setData([
                    "name" : self.nameInput.text ?? "",
                    "lastName" : self.lastInput.text ?? "",
                    "role" : "client",
                    "phone" : self.phoneInput.text ?? "",
                    "email" : self.emailInput.text ?? "",
                    "age" : 21
                ]) { err in
                    if let err = err {
                        print("Error writing document: \(err)")
                        return
                    }
                    self.login()
                }
            } else {
                print(error.debugDescription)
                return
            }
        }
        
    }
    
    @IBAction func goToLogin(_ sender: UIButton) {
        self.login()
    }
    func login(){
        let controller:LoginController? = self.navigationController?.storyboard?.instantiateViewController(identifier: "loginController")
        navigationController?.pushViewController(controller ?? LoginController(), animated: true)
    }
}
