//
//  HistoryController.swift
//  QPark
//
//  Created by Nico Gaitan on 10/10/19.
//  Copyright © 2019 Grupo11. All rights reserved.
//

import UIKit
import Firebase

class ParkingController: UITableViewController {
    let db = Firestore.firestore()
    var data: [QueryDocumentSnapshot]!
    var email: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        getParkings()
        self.tableView.reloadData()
        print("parkings \(data?.count ?? 0)")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "parkingCell", for: indexPath) as! ParkingCellController
        cell.parking = data[indexPath.row]
        cell.address!.text = data[indexPath.row].get("address") as? String
        cell.name!.text = data[indexPath.row].get("name") as? String
        return cell
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data?.count ?? 0
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let parkingInfo = (tableView.cellForRow(at: indexPath) as! ParkingCellController).parking
        let controller:ParkingDetailController? = self.navigationController?.storyboard?.instantiateViewController(identifier: "parkingDetailController")
        controller!.parking = parkingInfo
        navigationController?.pushViewController(controller ?? ParkingDetailController(), animated: true)
    }
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            self.data.remove(at: indexPath.row)
            self.remove(id: (tableView.cellForRow(at: indexPath) as! ParkingCellController).parking.documentID)
            tableView.reloadData()
        }
    }
    @IBAction func popMenu(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    func getParkings(){
        let email = (UserDefaults().dictionary(forKey: "user") as AnyObject).object(forKey: "email") as? String ?? "none"
        db.collection("parkings").whereField("owner", isEqualTo: email).getDocuments(completion:{ (querySnapshot, err) in
            if let error = err {
                print("Error getting documents: \(error)")
                return
            } else {
                self.data = querySnapshot!.documents
                self.tableView.reloadData()
                print("after response  \(self.data?.count ?? 0)")
            }
        })
    }
    func remove(id: String){
        db.collection("parkings").document(id).delete() {
            err in if let error = err{
                print("Error removing document: \(error)")
                return
            }else {
                print("Document successfully removed!")
            }
        }
    }
    @IBAction func popParkingList(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
