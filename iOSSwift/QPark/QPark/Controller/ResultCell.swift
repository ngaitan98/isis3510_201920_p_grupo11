//
//  ResultCell.swift
//  QPark
//
//  Created by Nico Gaitan on 10/14/19.
//  Copyright © 2019 Grupo11. All rights reserved.
//

import UIKit
import GooglePlaces

class ResultCell: UITableViewCell {
        
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var place: UILabel!
    
    var location:CLLocationCoordinate2D!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.frame.size.height = 80
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    
}
