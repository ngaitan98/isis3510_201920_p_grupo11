//
//  LoginController.swift
//  QPark
//
//  Created by Nico Gaitan on 10/10/19.
//  Copyright © 2019 Grupo11. All rights reserved.
//

import UIKit
import Firebase
import Network
class LoginController:UIViewController, UITextFieldDelegate{
    let db = Firestore.firestore()
    let localDb = UserDefaults.init()
    let monitor = NWPathMonitor()
    
    @IBOutlet weak var loginInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    
    @IBOutlet weak var lblNotification: UILabel!
    var label:UILabel!
    static var conn:Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNotification.isHidden = true
        
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                print("Yey")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "conn"), object: nil)
                self.UI {
                    if LoginController.conn != nil, !LoginController.self.conn!{
                        if self.label != nil {
                            self.hideToast(toast: self.label!)
                        }
                        LoginController.conn = true
                    }
                    LoginController.conn = true
                }
            } else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "noConn"), object: nil)
                self.UI {
                    LoginController.conn = false
                    self.label = self.showToast()
                }
            }
        }
        if localDb.bool(forKey: "userLogged"){
            self.goToRoot()
        }
        
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer( target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false; view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
        if localDb.bool(forKey: "userLogged"){
            self.goToRoot()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    @IBAction func loginWithEmail(_ sender: UIButton) {
        let email = loginInput.text
        let password = passwordInput.text
        if email != nil && email != "" && password != nil && password != ""{
            login(withEmail:email ?? "",password:password ?? "")
        }
        else if email == nil && email == "" && password == nil && password == ""{
            self.lblNotification.isHidden = false
            self.lblNotification.text = "Ingrese un correo y una contraseña."
        }
        else if email != nil && email != ""{
            self.lblNotification.isHidden = false
            self.lblNotification.text = "Ingrese un correo."
        }
        else{
            self.lblNotification.isHidden = false
            self.lblNotification.text = "Ingrese una contraseña."
        }
        
    }
    func login(withEmail email: String, password: String, _ callback: ((Error?) -> ())? = nil){
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if  let e  = error{
                print("Login Error: \(e)")
                if let errCode = AuthErrorCode(rawValue: error!._code) {
                    let alert:UIAlertController?
                    switch errCode{
                    case .invalidEmail:
                        alert = UIAlertController(title: "Error", message: "El formato de correo electrónico es inválido.", preferredStyle: .alert)
                        break
                    case .userNotFound:
                        alert = UIAlertController(title: "Error", message: "No existe un usuario con este correo.", preferredStyle: .alert)
                        break
                    case .wrongPassword:
                        alert = UIAlertController(title: "Error", message: "La contraseña es incorrecta.", preferredStyle: .alert)
                        break
                    case .networkError:
                        alert = UIAlertController(title: "Error", message: "No hay conexión a Internet", preferredStyle: .alert)
                    default:
                        alert = UIAlertController(title: "Error", message: "Hubo un error en la autenticación.", preferredStyle: .alert)
                        break
                    }
                    alert!.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alert!, animated: true, completion: nil)
                }
                
                return
            }
            self.db.collection("users").document(email).getDocument(){
                (document, error) in
                if let doc = document{
                    self.localDb.set(true, forKey: "userLogged")
                    self.localDb.set(doc.data(), forKey: "user")
                    self.goToRoot()
                    callback?(nil)
                }
                else{
                    do {
                        try Auth.auth().signOut()
                    } catch let signOutError as NSError {
                        print ("Error signing out: %@", signOutError)
                    }
                    print("Login Error: \(String(describing: error)))")
                }
                
            }
            
        }
    }
    func goToRoot(){
        let controller:RootController? = self.navigationController?.storyboard?.instantiateViewController(identifier: "rootController")
        navigationController?.pushViewController(controller ?? RootController(), animated: true)
    }
    @IBAction func goToRegister(_sender: UIButton){
        let controller:RegisterController? = self.navigationController?.storyboard?.instantiateViewController(identifier: "registerController")
        navigationController?.pushViewController(controller ?? RegisterController(), animated: true)
    }
}
extension UIViewController {
    
    func showToast() -> UILabel {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.origin.x + 20, y: self.view.frame.origin.y + 40, width: self.view.frame.width - 40, height: 30))
        toastLabel.backgroundColor = .systemRed
        toastLabel.textColor = UIColor.white
        toastLabel.font = UIFont(name: "AvenirNext", size: 15)
        toastLabel.textAlignment = .center;
        toastLabel.text = "No hay conexión a internet."
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 0;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        return toastLabel
    }
    
    func hideToast(toast: UILabel) {
        print("Should Hide")
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toast.alpha = 0.0
        }, completion: {(isCompleted) in
            toast.removeFromSuperview()
        })
    }
    
    func BG(_ block: @escaping ()->Void) {
        DispatchQueue.global(qos: .default).async(execute: block)
    }
    
    func UI(_ block: @escaping ()->Void) {
        DispatchQueue.main.async(execute: block)
    }
    
}

