//
//  ServiceCellController.swift
//  QPark
//
//  Created by Nico Gaitan on 10/19/19.
//  Copyright © 2019 Grupo11. All rights reserved.
//

import UIKit
import Firebase

class ServiceCellController: UITableViewCell {
    
    let db = Firestore.firestore()
    
    var service:QueryDocumentSnapshot!
    var user: DocumentSnapshot!
    var parking: DocumentSnapshot!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var endDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func GetUser(from service:QueryDocumentSnapshot){
        let user = service.get("user") as! String
        db.collection("users")
            .document(user)
            .getDocument(){
                (document, error) in
                if let err = error{
                    print(err)
                    return
                }
                else{
                    self.user = document
                }
        }
    }
    func GetParking(from service:QueryDocumentSnapshot){
        db.collection("parkings").document(service.get("parking") as! String).getDocument(){
            (document, error) in
            if let err = error{
                print(err)
                return
            }
            else{
                let formater = DateFormatter()
                formater.dateFormat = "dd/MM/yyyy HH:mm"
                let startDate = self.service!.get("startHour") as? Timestamp
                let endDate = self.service!.get("endHour") as? Timestamp
                
                let start = formater.string(from: startDate!.dateValue())
                var end: String!
                if let endValue = endDate{
                   end = formater.string(from: endValue.dateValue())
                }
                self.parking = document
                self.name?.text = self.parking.get("name") as? String
                self.address?.text = self.parking!.get("address") as? String
                self.startDate.text = "Inicio: \(start)"
                self.endDate.text = "Fin: \(end ?? "En curso")"
            }
        }
    }
}
