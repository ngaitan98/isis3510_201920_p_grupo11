//
//  parkingDetailController.swift
//  QPark
//
//  Created by Nico Gaitan on 10/18/19.
//  Copyright © 2019 Grupo11. All rights reserved.
//

import UIKit
import Firebase

class ParkingDetailController: UIViewController {
    
    var parking: QueryDocumentSnapshot!
    var servicesList: QuerySnapshot!
    let db = Firestore.firestore()
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var localidad: UILabel!
    @IBOutlet weak var zip: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        name?.text = parking!.get("name") as? String
        address?.text = parking!.get("address") as? String
        localidad?.text = parking!.get("localidad") as? String
        zip?.text = parking!.get("zipcode") as? String
        price?.text = String((parking!.get("costPerMinute"))as! Int)
    }
    
    @IBAction func pushServiceHistory(){

            db.collection("services").whereField("parking", isEqualTo: parking!.documentID).getDocuments(completion:{ (querySnapshot, err) in
                if let error = err {
                    print("Error getting documents: \(error)")
                    return
                } else {
                    let controller:ServiceController = self.navigationController?.storyboard?.instantiateViewController(identifier: "serviceController") as! ServiceController
                    controller.data = querySnapshot!.documents
                    self.navigationController?.pushViewController(controller, animated: true)
                }
            })
    }
    @IBAction func deleteParking(){
        db.collection("parkings").document(parking!.documentID).delete() {
            err in if let error = err{
                print("Error removing document: \(error)")
                return
            }else {
                print("Document successfully removed!")
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
