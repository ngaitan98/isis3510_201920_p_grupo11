//
//  MenuOptionController.swift
//  QPark
//
//  Created by Nico Gaitan on 10/16/19.
//  Copyright © 2019 Grupo11. All rights reserved.
//

import UIKit
import Foundation

class MenuOptionController: UITableViewCell {

    @IBOutlet weak var option: UILabel!
    
    @IBOutlet weak var optionImage: UIImageView!
    
    var opensView: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        print(self.contentView.frame)
    }
    

}
