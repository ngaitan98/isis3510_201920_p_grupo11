//
//  ServiceController.swift
//  QPark
//
//  Created by Nico Gaitan on 10/19/19.
//  Copyright © 2019 Grupo11. All rights reserved.
//

import UIKit
import Firebase

class ServiceController: UITableViewController {
    var data:[QueryDocumentSnapshot]!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return data?.count ?? 0
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "serviceCell", for: indexPath) as! ServiceCellController
        cell.service = data[indexPath.row]
        cell.GetParking(from: data[indexPath.row])
        cell.GetUser(from: data[indexPath.row])
        return cell
    }
    @IBAction func popView(){
        self.navigationController?.popViewController(animated: true)
    }
}
