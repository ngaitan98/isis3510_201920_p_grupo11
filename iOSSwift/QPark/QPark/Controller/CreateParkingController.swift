//
//  CreateParkingController.swift
//  QPark
//
//  Created by Nico Gaitan on 10/22/19.
//  Copyright © 2019 Grupo11. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation

class CreateParkingController: UIViewController {
    
    let db = Firestore.firestore()
    let locationManager = CLLocationManager()
    @IBOutlet weak var nameInput: UITextField!
    @IBOutlet weak var addressInput: UITextField!
    @IBOutlet weak var priceInput: UITextField!
    @IBOutlet weak var localidadInput: UITextField!
    @IBOutlet weak var zipInput: UITextField!
    @IBOutlet weak var cuposInput: UITextField!
    var toastLabel:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(noConnection), name: NSNotification.Name(rawValue: "noConn"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(connection), name: NSNotification.Name(rawValue: "conn"), object: nil)
    }
    @objc func noConnection() {
        DispatchQueue.main.async {
            self.toastLabel=self.showToast()
        }
    }
    
    @objc func connection() {
        if let lbl = toastLabel{
            DispatchQueue.main.async {
                self.hideToast(toast: lbl)
            }
        }
    }
    @IBAction func createParking(_ sender: UIButton){
        let name = nameInput?.text
        let address = addressInput?.text
        let price = priceInput?.text
        let zip = zipInput?.text
        let cupos = cuposInput?.text
        let localidad = localidadInput?.text
        let location = locationManager.location!.coordinate
        if name == nil || name == ""{
            //TODO Handle
            return
        }
        if address == nil || address == ""{
            //TODO Handle
            return
        }
        if zip == nil || zip == ""{
            //TODO Handle
            return
        }
        if price == nil || price == ""{
            //TODO Handle
            return
        }
        if cupos == nil || cupos == ""{
            //TODO Handle
            return
        }
        if localidad == nil || localidad == ""{
            //TODO Handle
            return
        }
        print("CHECK 1")
        self.db.collection("parkings").document().setData([
            "name" : name!,
            "address" :address!,
            "zipCode" : Int(zip!)!,
            "costPerMinute" : Int(price!)!,
            "localidad" : localidad!,
            "parkingSpots" : Int(cupos!)!,
            "owner" : (UserDefaults().dictionary(forKey: "user") as AnyObject).object(forKey: "email")!,
            "location" : GeoPoint(latitude: location.latitude, longitude: location.longitude)
        ]) { err in
            print("CHECK 1")
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                self.navigationController?.popViewController(animated: true)
                print("Document successfully written!")
            }
        }
    }
    @IBAction func popCreate(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
}
