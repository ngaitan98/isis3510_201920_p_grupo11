//
//  ProfileController.swift
//  QPark
//
//  Created by Nico Gaitan on 10/10/19.
//  Copyright © 2019 Grupo11. All rights reserved.
//

import UIKit

class ProfileController: UIViewController {
    
    let localDb = UserDefaults()
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var email: UILabel!
    
    @IBOutlet weak var lastName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let user = localDb.dictionary(forKey: "user") as AnyObject? ?? nil!
        name.text = "\((user).object(forKey: "name") ?? "John") \((user).object(forKey: "lastName") ?? "Doe")"
        email.text = "\((user).object(forKey: "email") ?? "j.doe@email.com")"
        lastName.text = "\((user).object(forKey: "phone") ?? "3111111111")"
        
    }
    
    @IBAction func popProfile(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
